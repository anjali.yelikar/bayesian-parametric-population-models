def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_mass_distribution")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot_intensity_m1")
    subparser.add_argument("output_plot_pdf_m1")
    subparser.add_argument("output_plot_intensity_q")
    subparser.add_argument("output_plot_pdf_q")

    subparser.add_argument(
        "--output-files",
        metavar=(
            "FILE_INTENSITY_M1", "FILE_PDF_M1",
            "FILE_INTENSITY_Q", "FILE_PDF_Q",
        ),
        nargs=4,
    )

    subparser.add_argument(
        "--isolate-subpops", nargs="+",
        help="Only plot specific subpopulations.  "
             "List their suffixes, e.g., 'pl0 g0 pl1'.",
    )

    subparser.add_argument("--n-plot-points", type=int, default=50)
    subparser.add_argument("--n-samples-per-realization", type=int, default=300)

    subparser.add_argument(
        "--m1-scale",
        choices=["linear", "log"], default="log",
    )

    subparser.add_argument(
        "--overlay-truth",
        help="JSON file specifying parameters for a population to overlay.",
    )

    subparser.add_argument(
        "--n-oom",
        type=int,
        help="Limit plots' y-axes to the specified number of OoM from the "
             "peak.",
    )

    subparser.add_argument(
        "--mass-range",
        type=float, nargs=2, default=[5.0, 100.0],
        help="Range of masses to plot, default is [5,100]Msun",
    )

    subparser.add_argument(
        "--density-estimator",
        default="hist", choices=["hist", "kde"],
        help="Density estimator to use.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.add_argument(
        "--skip-mass-ratio",
        action="store_true",
        help="Do not produce mass ratio plot (costly operation, often unused).",
    )

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Overwrite existing HDF5 output files.",
    )

    subparser.add_argument(
        "--debug",
        nargs="*",
        help="Output debugging messages.  Use with no arguments to display all "
             "debugging information, or specify the debugging mode(s).",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from pop_models.posterior import H5CleanedPosteriorSamples
    from pop_models.coordinate import CoordinateSystem
    from pop_models.credible_regions import CredibleRegions1D
    from pop_models.astro_models.coordinates import (
        m1_source_coord, m2_source_coord, q_coord,
    )
    from pop_models.utils.plotting import limit_ax_oom
    from pop_models.utils import debug_verbose

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats

    # Set debugging mode
    if cli_args.debug is not None:
        if len(cli_args.debug) == 0:
            debug_verbose.full_output_on()
        else:
            debug_verbose.enable_modes(*cli_args.debug)

    random_state = numpy.random.RandomState(cli_args.seed)

    # Determine whether HDF5 outputs are being made, and pull out filenames
    hdf5_out = cli_args.output_files is not None
    if hdf5_out:
        (
            output_file_intensity_m1, output_file_pdf_m1,
            output_file_intensity_q, output_file_pdf_q,
        ) = cli_args.output_files

        hdf5_mode = "w" if cli_args.force else "w-"

    # Load in true parameters if provided
    if cli_args.overlay_truth:
        import json
        with open(cli_args.overlay_truth, "r") as truths_file:
            parameters_true = json.load(truths_file)
        # Convert scalars into single-element numpy arrays
        parameters_true = {
            name : numpy.asarray(value)
            for name, value in parameters_true.items()
        }

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Get appropriate population object.
        metadata = post_samples.metadata
        n_powerlaws = metadata["n_powerlaws"]
        n_gaussians = metadata["n_gaussians"]
        M_max = metadata["M_max"]
        same_gauss_mass_cutoffs = metadata.get("same_gauss_mass_cutoffs", True)
        population = get_population(
            n_powerlaws, n_gaussians, M_max,
            same_gauss_mass_cutoffs=same_gauss_mass_cutoffs,
        )
        # Isolate sub-populations if requested.
        if cli_args.isolate_subpops is not None:
            # Add in the underscore prefixes for convenience.
            suffixes = ["_"+s for s in cli_args.isolate_subpops]
            # Replace population with sub-populations.
            population = population.get_subpops(suffixes)

        # Split into p(m1) and p(q)
        population_m1 = population.to_coords(CoordinateSystem(m1_source_coord))
        population_q = population.to_coords(CoordinateSystem(q_coord))

        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Determine m_min and m_max
        ## TODO: Actually figure out from posterior samples
        m_min, m_max = cli_args.mass_range
        # Determine q_min and q_max
        ## TODO: Actually figure out from posterior samples
        q_min, q_max = 1e-4, 1.0

        # Create wrapper functions for the PDF's and intensity functions, which
        # currently need to be built up with KDE's from samples, as there aren't
        # explicit mass marginals available right now.
        def m1_pdf(m1, parameters):
            return population_m1.pdf(
                (m1,), parameters,
            )
        def m1_intensity(m1, parameters):
            return (
                population.normalization(parameters).T *
                m1_pdf(m1, parameters).T
            ).T

        def q_pdf(q, parameters):
            q_samples, = population_q.rvs(
                cli_args.n_samples_per_realization, parameters,
                random_state=random_state,
            )

            obs_shape = q.shape
            params_shape = q_samples.shape[:-1]
            out_shape = params_shape + obs_shape

            pdf = numpy.empty(out_shape, dtype=numpy.float64)

            # Compute same bins for all histograms, the "hist" method is used.
            if cli_args.density_estimator == "hist":
                n_samples = q_samples.shape[-1]
                IQR = scipy.stats.iqr(q_samples)
                h = 2 * IQR * n_samples**(-1.0/3.0)
                bin_edges = numpy.arange(q_min, q_max+h, h)

            for idx in numpy.ndindex(*params_shape):
                if cli_args.density_estimator == "hist":
                    hist, _ = numpy.histogram(
                        q_samples[idx],
                        bins=bin_edges, density=True,
                    )
                    # Add on zero bins outside of histogram range.
                    hist = numpy.concatenate(([0.0], hist, [0.0]))
                    bin_loc = numpy.digitize(q, bin_edges)
                    pdf[idx] = hist[numpy.digitize(q, bin_edges)]
                elif cli_args.density_estimator == "kde":
                    q_kde = scipy.stats.gaussian_kde(
                        q_samples[idx],
                        bw_estimator="scott",
                    )
                    pdf[idx] = q_kde(q)
                else:
                    raise KeyError(
                        "Unknown density estimator: {}"
                        .format(cli_args.density_estimator)
                    )

            return pdf

        def q_intensity(q, parameters):
            return (
                population.normalization(parameters).T *
                q_pdf(q, parameters).T
            ).T

        ## Plot m1
        if cli_args.m1_scale == "linear":
            m1_grid = numpy.linspace(m_min, m_max, cli_args.n_plot_points)
        else:
            m1_grid = numpy.logspace(
                numpy.log10(m_min), numpy.log10(m_max),
                cli_args.n_plot_points,
            )
        ## TODO: plot intensity

        m1_pdf_ci = CredibleRegions1D.from_samples(
            m1_pdf, m1_grid, parameter_samples,
        )
        m1_intensity_ci = CredibleRegions1D.from_samples(
            m1_intensity, m1_grid, parameter_samples,
        )

        fig_pdf, ax_pdf = plt.subplots(figsize=[6,4])
        fig_intensity, ax_intensity = plt.subplots(figsize=[6,4])

        m1_pdf_ci.plot(
            ax_pdf,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Overlay truths if provided
        if cli_args.overlay_truth:
            ax_pdf.plot(
                m1_grid, m1_pdf(m1_grid, parameters_true),
                color="C3",
            )

        # Save to file if provided
        if hdf5_out:
            m1_pdf_ci.save(output_file_pdf_m1, mode=hdf5_mode)
        m1_intensity_ci.plot(
            ax_intensity,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Overlay truths if provided
        if cli_args.overlay_truth:
            ax_intensity.plot(
                m1_grid, m1_intensity(m1_grid, parameters_true),
                color="C3",
            )
        # Save to file if provided
        if hdf5_out:
            m1_intensity_ci.save(output_file_intensity_m1, mode=hdf5_mode)

        ## TODO: implement synthetic truth overlay

        ax_pdf.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax_intensity.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax_pdf.set_ylabel(r"$p(m_{1,\mathrm{source}} / M_\odot)$")
        ax_intensity.set_ylabel(
            r"$\mathcal{R} \, p(m_{1,\mathrm{source}} / M_\odot)$"
        )

        if cli_args.m1_scale == "log":
            ax_pdf.set_xscale("log")
            ax_intensity.set_xscale("log")
        ax_pdf.set_yscale("log")
        ax_intensity.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax_pdf, cli_args.n_oom, "y")
            limit_ax_oom(ax_intensity, cli_args.n_oom, "y")

        fig_pdf.savefig(cli_args.output_plot_pdf_m1)
        fig_intensity.savefig(cli_args.output_plot_intensity_m1)
        # Free up memory
        plt.close(fig_pdf)
        plt.close(fig_intensity)
        del m1_grid, m1_pdf_ci


        # Skip plotting mass ratio if requested
        if cli_args.skip_mass_ratio:
            return

        ## Plot q
        q_grid = numpy.linspace(q_min, q_max, cli_args.n_plot_points)
        ## TODO: plot intensity

        q_pdf_ci = CredibleRegions1D.from_samples(
            q_pdf, q_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        q_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Overlay truths if provided
        if cli_args.overlay_truth:
            ax_pdf.plot(
                q_grid, q_pdf(q_grid, parameters_true),
                color="C3",
            )

        # Save to file if provided
        if hdf5_out:
            q_pdf_ci.save(output_file_pdf_q, mode=hdf5_mode)

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_2 / m_1$")
        ax.set_ylabel(r"$p(q)$")

        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_pdf_q)
