import warnings
import numpy

__xpy = numpy
__is_locked = False


def use_numpy():
    global __xpy
    if not __is_locked:
        import numpy as __xpy
    else:
        warnings.warn("Backend is locked, cannot set to numpy.")


def use_cupy():
    global __xpy
    if not __is_locked:
        try:
            import cupy as __xpy
        except ImportError as e:
            raise e # TODO: make custom message
    else:
        warnings.warn("Backend is locked, cannot set to cupy.")


def get_xpy(untouched=False):
    if not untouched:
        __is_locked = True

    return __xpy


def asnumpy(arr):
    """
    Converts an array from xpy to numpy.  Changes nothing if xpy = numpy.
    """
    if __xpy == numpy:
        return arr
    else:
        return __xpy.asnumpy(arr)


def asxpy(arr):
    """
    Converts an array from numpy to xpy.  Changes nothing if xpy = numpy.
    """
    if __xpy == numpy:
        return arr
    else:
        return __xpy.asarray(arr)
