__all__ = [
    "Population",
    "TransformedPopulation",
    "ReorderedPopulation",
    "SeparableCoordinatePopulation",
    "MixturePopulation",
    "ConditionalPopulation",
]

import typing
import warnings

import numpy

from pop_models.utils import debug_verbose

from pop_models.coordinate import (
    Coordinate,
    CoordinateSystem, coord_nil,
    CoordinateTransformFunction, CoordinateTransforms, transformations_nil,
    CorrelatedCoords,
)
from pop_models.types import (
    Numeric, WhereType, Observables, Parameters,
    NumericSingle, ParametersSingle,
)



class Population(object):
    r"""
    Abstract base class for representing parameterized populations of objects.
    Implementing sub-classes should represent the distribution of objects in
    that population through their probability distribution, and their overall
    occurence rate in the search space.

    The probability distribution is represented by the :py:meth:`pdf` (for
    evaluating the probability density) and :py:meth:`rvs` (for drawing random
    samples) methods.  Any useful sub-class will implement at least one of these
    two methods (depending on your use case, you may only need one).

    The overall occurence rate is represented by the :py:meth:`normalization`
    method, which sub-classes are required to implement.

    The product of :py:meth:`pdf` and :py:meth:`normalization` is produced by
    the :py:meth:`intensity` method.

    The :py:meth:`pdf` and :py:meth:`intensity` methods are assumed to evaluate
    the distribution in a specific coordinate system, which must be given to the
    constructor.  Can be transformed to other coordinate systems by calling the
    :py:meth:`to_coords` method with a coordinate system that's understood.
    Transformations are available through the :py:class:`CoordinateTransforms`
    object that is passed to the constructor, as well as anything you implement
    explicitly by overriding :py:meth:`to_coords`.
    """
    def __init__(
            self,
            coord_system: CoordinateSystem,
            param_names: typing.Tuple[str,...],
            transformations: CoordinateTransforms=transformations_nil,
            correlated_coords: CorrelatedCoords=frozenset(),
        ) -> None:
        self._coord_system = coord_system
        self._param_names = param_names
        self._transformations = transformations

        # Store set of correlated coordinates, removing any redundant sets.
        correlated_coords_mutable_copy = set(correlated_coords)
        for coord_set_input in correlated_coords:
            for coord_set_remaining in correlated_coords_mutable_copy:
                # Remove ``coord_set_input`` the mutable copy if it is a strict
                # subset of an element already present there.
                if coord_set_input < coord_set_remaining:
                    correlated_coords_mutable_copy.remove(coord_set_input)
                    break
        self._correlated_coords = frozenset(correlated_coords_mutable_copy)

    @property
    def coord_system(self) -> CoordinateSystem:
        return self._coord_system

    @property
    def param_names(self) -> typing.Tuple[str,...]:
        return self._param_names

    @property
    def transformations(self) -> CoordinateTransforms:
        return self._transformations

    @property
    def correlated_coords(self) -> CorrelatedCoords:
        return self._correlated_coords

    def pdf_single(
            self,
            observables: Observables,
            parameters: ParametersSingle,
            **kwargs
        ) -> Numeric:
        raise NotImplementedError

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        r"""
        Returns the probability density evaluated at the chosen observables
        :math:`\lambda`, for the assumed population parameters :math:`\Lambda`.

        Observables are specified as a sequence of components of
        :math:`\lambda`, and must be provided in the same order as they appear
        in :py:attr:`coord_system`.  Parameters are specified as a mapping of
        parameter names to their associated values.  All can be given as either
        scalars or as NumPy arrays, so long as there is internal consistency in
        the shapes of the components of ``observables``,, as well as the values
        of ``parameters`` (though ``observables`` and ``parameters`` may be
        different from each other).  The output's shape is the combination of
        the two: the shape of the parameters plus the shape of the observables.

        An optional argument, ``where``, allows you to specify indices in
        ``parameters`` that you do not intend to look at, and can leave the
        corresponding indices in output uninitialized.
        """
        obs_shape = numpy.shape(observables[0])
        params_shape = numpy.shape(next(iter(parameters)))
        output_shape = params_shape + obs_shape

        where_arr = numpy.broadcast_to(where, params_shape)

        p = numpy.zeros(output_shape, dtype=numpy.float64)

        for idx in numpy.ndindex(*params_shape):
            if not where_arr[idx]:
                continue

            p[idx] = self.pdf_single(
                observables, parameters,
                **kwargs
            )

        return p

    def rvs_single(
            self,
            n_samples: int,
            parameters: ParametersSingle,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        raise NotImplementedError()

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        params_shape = numpy.shape(next(iter(parameters.values())))
        output_shape = params_shape + (n_samples,)

        parameters_arr = {
            name : numpy.asarray(values)
            for name, values in parameters.items()
        }
        where_arr = numpy.broadcast_to(where, params_shape)

        observables = tuple(
            numpy.full(
                output_shape,
                fill_value=coord.fill_value, dtype=numpy.float64,
            )
            for coord in self.coord_system
        )

        for idx in numpy.ndindex(*params_shape):
            if not where_arr[idx]:
                continue

            parameters_at_idx = {
                name : values[idx]
                for name, values in parameters_arr.items()
            }

            observables_single = self.rvs_single(
                n_samples, parameters_at_idx,
                random_state=random_state,
                **kwargs
            )
            for obs_out, obs in zip(observables, observables_single):
                obs_out[idx] = obs

        return observables


    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        raise NotImplementedError()

    def intensity(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return numpy.transpose(
            numpy.transpose(self.normalization(parameters, where=where)) *
            numpy.transpose(self.pdf(observables, parameters, where=where))
        )

    def params_valid(self, parameters: Parameters) -> WhereType:
        """
        Returns a boolean array specifying where the parameters are valid.
        """
        params_shape = numpy.shape(next(iter(parameters.values())))

        return numpy.broadcast_to(True, params_shape)

    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> "Population":
        if self.coord_system == coord_system:
            return self

        # Remaining options

        # Try a simple re-ordering
        reordering = self.coord_system.find_reordering(coord_system)
        if reordering is not None:
            return ReorderedPopulation(
                self, coord_system, reordering, self.transformations,
            )

        # If not allowing use of transformations, we've failed.
        if not use_transformations:
            raise ValueError(
                self._unknown_coord_transform_msg(
                    self.coord_system, coord_system,
                )
            )
        # Try using a known coordinate transformation
        # TODO: Need Jacobians to make transformed `pdf` method work
        transf_func = self.transformations[self.coord_system, coord_system]
        return TransformedPopulation(
            self, coord_system, transf_func,
            transformations=self.transformations,
        )

        # raise ValueError(
        #     self._unknown_coord_transform_msg(self.coord_system, coord_system)
        # )

    @staticmethod
    def _unknown_coord_transform_msg(
            c1: CoordinateSystem, c2: CoordinateSystem,
        ) -> str:
        return (
            "No known method to convert from coordinate system "
            "{c1} to {c2}"
            .format(c1=c1, c2=c2)
        )

    def corrcoef(
            self,
            parameters: Parameters,
            n_samples: int=1000,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
        ) -> numpy.ndarray:
        """
        Computes the correlation Pearson product-moment correlation coefficient
        matrix via Monte Carlo, for every pair of observables in this
        :py:class:`Population`'s coordinate system.  Object must have an
        :py:class:`rvs` method.  For better/worse precision, increase/decrease
        the ``n_samples`` argument.
        """
        # Determine shape of inputs/outputs.
        n_obs = len(self.coord_system)
        param_shape = numpy.asarray(next(iter(parameters.values()))).shape
        out_shape = param_shape + (n_obs, n_obs)
        # Pre-allocate output array.
        corr_matrix = numpy.empty(out_shape)
        # Broadcast 'where' array
        where_arr = numpy.broadcast_to(numpy.asarray(where), param_shape)

        # Draw random samples from the population
        observable_samples = self.rvs(
            n_samples, parameters,
            where=where, random_state=random_state,
        )

        # Iterate over each set of parameters, and compute the correlation
        # coefficients.
        for idx in numpy.ndindex(*param_shape):
            # Skip unwanted samples.
            if not where_arr[idx]:
                continue
            # Compute and store the correlation coefficients
            corr_matrix[idx] = numpy.corrcoef([
                typing.cast(numpy.ndarray, obs)[idx]
                for obs in observable_samples
            ])

        # Return correlation coefficients
        return corr_matrix


class TransformedPopulation(Population):
    def __init__(
            self,
            source_population: Population,
            coord_system: CoordinateSystem,
            transform_func: CoordinateTransformFunction,
            transformations: CoordinateTransforms=transformations_nil,
        ):
        ## TODO: need to actually derive from transformations
        warnings.warn(
            "TransformedPopulation does not yet support determination of "
            "which transformed coordinates are correlated.  Assuming all are "
            "correlated as a fallback."
        )
        correlated_coords = frozenset({
            frozenset(coord for coord in coord_system)
        })
        super().__init__(
            coord_system, source_population.param_names,
            transformations=transformations,
            correlated_coords=correlated_coords,
        )

        self.source_population = source_population
        self.transform_func = transform_func

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        samples_untransformed = self.source_population.rvs(
            n_samples, parameters, where=where, random_state=random_state,
            **kwargs
        )
        return self.transform_func(samples_untransformed)

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return self.source_population.normalization(
            parameters, where=where, **kwargs,
        )

class ReorderedPopulation(Population):
    def __init__(
            self,
            source_population: Population,
            coord_system: CoordinateSystem,
            reordering: typing.List[int],
            transformations: CoordinateTransforms=transformations_nil,
        ):
        super().__init__(
            coord_system, source_population.param_names,
            transformations=transformations,
            correlated_coords=source_population.correlated_coords,
        )

        self.source_population = source_population
        self.reordering = reordering

    def pdf(
            self,
            observables: Observables, parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return self.source_population.pdf(
            tuple(observables[i] for i in self.reordering),
            parameters,
            where=where,
            **kwargs
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        samples = self.source_population.rvs(
            n_samples, parameters, where=where, random_state=random_state,
            **kwargs
        )
        samples_copy = [s for s in samples]
        for i, j in enumerate(self.reordering):
            samples_copy[j] = samples[i]

        return tuple(samples_copy)

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return self.source_population.normalization(
            parameters, where=where, **kwargs,
        )

    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> Population:
        return self.source_population.to_coords(
            coord_system, use_transformations=use_transformations,
        )


class SeparableCoordinatePopulation(Population):
    r"""
    Define a :py:class:`Population` by combining multiple separable building
    block populations, with non-overlapping coordinates.  This assumes that the
    observables from each building block population are completely independent
    of those from any other.

    Simply pass a list of :py:class:`Population` objects you'd like to combine.
    Note that the :py:meth:`normalization` method will simply call the method
    from the first listed population.
    """
    def __init__(
            self,
            *separable_parts: Population,
            transformations: CoordinateTransforms=transformations_nil
        ):
        if len(separable_parts) == 0:
            raise ValueError(
                "Cannot initialize a SeparableCoordinatePopulation without "
                "at least one Population object provided as input."
            )

        # Store the separable parts.  If any of the separable parts are
        # themselves `SeparableCoordinatePopulation` objects, they will be split
        # apart.
        self.separable_parts = []
        for pop in separable_parts:
            if isinstance(pop, SeparableCoordinatePopulation):
                self.separable_parts += pop.separable_parts
            else:
                self.separable_parts.append(pop)

        # Keep track of all parts' coordinate systems.
        self.separable_coord_systems = [
            part.coord_system for part in self.separable_parts
        ]

        # Coordinate system is the combination of all parts' coordinate systems.
        coord_system = sum(
            self.separable_coord_systems,
            coord_nil,
        )
        # Cannot have any coordinates repeated, ensure this is true.
        if len(set(coord_system)) != len(coord_system):
            raise ValueError(
                "Parts include a duplicate coordinate."
            )

        param_names_list = [] # type: typing.List[str]
        for pop in self.separable_parts:
            for name in pop.param_names:
                if name not in param_names_list:
                    param_names_list.append(name)
        param_names = tuple(param_names_list)

        correlated_coords = frozenset.union(
            *(pop.correlated_coords for pop in self.separable_parts)
        )

        super().__init__(
            coord_system, param_names,
            transformations=transformations,
            correlated_coords=correlated_coords,
        )

        # Pre-compute a list of start and end indices for input observables,
        # based on the order and length of the coordinates.  This saves the need
        # to re-compute it every time a method like `pdf` is called.
        i = 0
        self._coord_indices = [] # type: typing.List[typing.Tuple[int, int]]
        for c in self.separable_coord_systems:
            start = i
            end = i + len(c)

            self._coord_indices.append((start, end))

            i = end

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        from functools import reduce
        import operator

        return reduce(
            operator.mul,
            (
                p.pdf(observables[start:end], parameters, where=where, **kwargs)
                for p, (start, end)
                in zip(self.separable_parts, self._coord_indices)
            ),
            1.0
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        return sum(
            (
                pop.rvs(
                    n_samples, parameters,
                    where=where, random_state=random_state,
                    **kwargs
                )
                for pop in self.separable_parts
            ),
            (),
        )

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        """
        Uses the 'normalization' method from the first separable part to get the
        normalization.  There is no meaningful way to combine normalizations
        from multiple separable parts.  The only sensible approach is to have
        them all produce the same result.
        """
        return self.separable_parts[0].normalization(
            parameters, where=where,
            **kwargs
        )

    def params_valid(self, parameters: Parameters) -> WhereType:
        from functools import reduce

        params_shape = numpy.shape(next(iter(parameters.values())))

        initial_valid = numpy.broadcast_to(True, params_shape)

        return reduce(
            numpy.logical_and,
            (pop.params_valid(parameters) for pop in self.separable_parts),
            initial_valid,
        )


    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> Population:
        from itertools import combinations

        if self.coord_system == coord_system:
            debug_verbose(
                "Undergoing trivial coordinate change.",
                mode="separable_pop", flush=True,
            )
            return self

        # Check if the desired coordinate system is contained within one of the
        # separable coordinate systems.  If so, return the corresponding
        # Population.
        iterables = zip(self.separable_coord_systems, self.separable_parts)
        for pop_coord_system, pop in iterables:
            if coord_system.issubset(pop.coord_system):
                debug_verbose(
                    "Found coordinate system in separable part,", pop,
                    mode="separable_pop", flush=True,
                )
                return pop.to_coords(coord_system)

        # Check if the desired coordinate system can be obtained by dropping one
        # or more of the separable coordinate systems.  If so, create a new
        # SeparableCoordinatePopulation using that subset of separable parts.
        # This would reproduce the case of a single separable part, handled in
        # the previous case, but that is already taken care of, so we skip the
        # n_parts = 1 case.
        n_total_parts = len(self.separable_parts)
        for n_parts in range(2, n_total_parts):
            for idx in combinations(range(n_total_parts), n_parts):
                # Get list of coordinates in this candidate subset.
                coord_subset = [self.separable_coord_systems[i] for i in idx]

                # Coordinate system subset matches the desired coordinates, but
                # possibly in a different order.  Make a
                # SeparableCoordinatePopulation object in the order the
                # coordinates appear for `self`, and then call `to_coords()` on
                # the new object to handle any possible re-ordering.
                if set(sum(coord_subset, coord_nil)) == set(coord_system):
                    # Get list of populations in this subset.
                    part_subset = [self.separable_parts[i] for i in idx]

                    debug_verbose(
                        "Transforming SeparableCoordinatePopulation with the "
                        "subset:", part_subset,
                        mode="separable_pop", flush=True,
                    )

                    pop = SeparableCoordinatePopulation(
                        *part_subset,
                    )
                    return pop.to_coords(
                        coord_system, use_transformations=use_transformations,
                    )

        # Check if the desired coordinate system can be derived from one of the
        # separable coordinate systems.  If so, return the corresponding derived
        # Population.
        for sub_pop in self.separable_parts:
            try:
                pop = sub_pop.to_coords(
                    coord_system, use_transformations=use_transformations,
                )
                debug_verbose(
                    "Transforming SeparableCoordinatePopulation by "
                    "transforming the separable part", sub_pop, "into", pop,
                    mode="separable_pop", flush=True,
                )
            except ValueError:
                pass
            except KeyError:
                pass

        # TODO:
        # Check if the desired coordinate system can be derived by a
        # SeparableCoordinatePopulation

        # Try to derive the coordinate system by methods available to the base
        # class.
        try:
            debug_verbose(
                "Falling back on base class' transformations.",
                mode="separable_pop", flush=True,
            )
            return super().to_coords(
                coord_system, use_transformations=use_transformations,
            )
        except ValueError:
            pass

        # All of the above failed.
        raise ValueError(
            self._unknown_coord_transform_msg(self.coord_system, coord_system)
        )


class MixturePopulation(Population):
    r"""
    Define a population as a mixture of multiple existing populations.  These
    to be specified by the ``suffix_to_population`` argument to the constructor,
    which should map to each sub-population a suffix string.  Any parameters
    passed to the resulting :py:class:`Population` object will be partitioned
    based on their suffices, and only passed to their specified sub-population,
    with the suffix stripped.

    For example, if you had two instances of the same :py:class:`Population`,
    which each expected parameters named "A" and "B", and the suffix mapping was
    defined as

    .. code-block:: python

       suffix_to_population = {
           "_1" : some_population_object,
           "_2" : some_population_object,
       }

    then the expected parameters would be

    .. code-block:: python

       parameters = {
           "A_1" : value_of_A1, "B_1" : value_of_B1,
           "A_2" : value_of_A2, "B_2" : value_of_B2,
       }

    which would then be automatically broken into two sets of parameters:

    .. code-block:: python

       parameters_to_pass_to_pop_1 = {
           "A" : value_of_A1, "B" : value_of_B1,
       }
       parameters_to_pass_to_pop_2 = {
           "A" : value_of_A2, "B" : value_of_B2,
       }

    so as far as the sub-populations can tell, they're being passed the
    parameters they expected.
    """
    def __init__(
            self,
            coord_system: CoordinateSystem,
            suffix_to_population: typing.Mapping[str, Population],
            transformations: CoordinateTransforms=transformations_nil,
        ):
        param_names = sum(
            (
                tuple(name+suffix for name in pop.param_names)
                for suffix, pop in suffix_to_population.items()
            ),
            (),
        )
        correlated_coords = frozenset.union(
            *(pop.correlated_coords for pop in suffix_to_population.values())
        )

        super().__init__(
            coord_system, param_names,
            transformations=transformations,
            correlated_coords=correlated_coords,
        )

        self.suffixes = list(suffix_to_population.keys())
        self.sub_populations = list(suffix_to_population.values())

        self._compute_sub_population_observables_indices()

        self._sub_population_parameter_name_pairs_initialized = False

        self._coord_system_set = set(self.coord_system)

        self._check_coord_systems_covered()

    def intensity(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        sub_population_observables = (
            self._compute_sub_population_observables(observables)
        )

        sub_population_parameters = (
            self._compute_sub_population_parameters(parameters)
        )

        iterables = zip(
            self.sub_populations,
            sub_population_observables,
            sub_population_parameters,
        )
        return sum(
            pop.intensity(obs, params, where=where, **kwargs)
            for pop, obs, params in iterables
        )

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        intensity = self.intensity(
            observables, parameters,
            where=where,
            **kwargs
        )
        norm = self.normalization(parameters, where=where, **kwargs)

        return numpy.transpose(
            numpy.transpose(intensity) / numpy.transpose(norm)
        )

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        sub_population_parameters = (
            self._compute_sub_population_parameters(parameters)
        )

        return sum(
            pop.normalization(params, where=where, **kwargs)
            for pop, params in
            zip(self.sub_populations, sub_population_parameters)
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            shuffle=False,
            **kwargs
        ) -> Observables:
        import scipy.stats

        # Determine shape of parameters arrays.
        shape_params = numpy.shape(next(iter(parameters.values())))
        # Output should be the same shape as the input parameters arrays, but
        # with an added axis of length `n_samples`.
        shape_output = shape_params + (n_samples,)

        where_arr = numpy.broadcast_to(where, shape_params)

        # Initialize output samples arrays.
        samples = [
            numpy.full(
                shape_output,
                fill_value=coord.fill_value, dtype=numpy.float64,
            )
            for coord in self.coord_system
        ]

        sub_population_parameters = (
            self._compute_sub_population_parameters(parameters)
        )

        norms = [
            numpy.asarray(pop.normalization(params, where=where_arr, **kwargs))
            for pop, params
            in zip(self.sub_populations, sub_population_parameters)
        ]
        total_norm = sum(norms)

        # Weights for multinomial distribution
        weights = [norm / total_norm for norm in norms]
        debug_verbose(
            "Mixure model has weights:", weights,
            mode="mixture_pop", flush=True,
        )

        # Iterate over choices of parameters
        for idx in numpy.ndindex(shape_params):
            # If `where` tells us to skip this element, then skip.
            if not where_arr[idx]:
                continue

            w = [ws[idx] for ws in weights]

            # Determine the number of draws to take from each sub-population.
            debug_verbose(
                "Determining sample count for sub-populations.",
                mode="mixture_pop", flush=True,
            )
            n_samples_per_subpop = scipy.stats.multinomial(
                n_samples, w,
            ).rvs(1, random_state=random_state)[0]
            debug_verbose(
                "Sample counts on index", idx, "are", n_samples_per_subpop,
                mode="mixture_pop", flush=True,
            )

            # Index to keep track of where we are in samples output array.
            n_drawn = 0

            # Draw samples from each sub-population.
            for i in range(len(self.sub_populations)):
                # Information on this specific sub-population.
                n = n_samples_per_subpop[i]
                pop = self.sub_populations[i]
                indices = self._sub_population_observables_indices[i]
                params = {
                    k: numpy.asarray(v)[idx]
                    for k, v in sub_population_parameters[i].items()
                }

                # Index for end of current set of samples.
                n_after_draw = n_drawn + n

                # Draw samples from this sub-population.
                debug_verbose(
                    "Drawing", n,
                    "samples from sub-population", self.suffixes[i],
                    mode="mixture_pop", flush=True,
                )
                samples_subpop = pop.rvs(
                    n, params,
                    where=where_arr[idx], random_state=random_state,
                    **kwargs
                )
                debug_verbose(
                    "Samples drawn successfully",
                    mode="mixture_pop", flush=True,
                )

                # Place the samples in the correct output arrays.
                for j, s in zip(indices, samples_subpop):
                    samples[j][idx][n_drawn:n_after_draw] = s

                # Starting index for next set of samples is where we left off.
                n_drawn = n_after_draw

            # Shuffle the samples if requested, so we don't know which
            # sub-population provided each sample.
            if shuffle:
                debug_verbose(
                    "Shuffling samples from mixture model",
                    mode="mixture_pop", flush=True,
                )
                # If no RandomState object was given, we need to make one now.
                if random_state is None:
                    random_state = numpy.random.RandomState()

                # Determine random sorting order.  Needs to be the same for all
                # variables so we can't use `numpy.random.shuffle`.
                i_sort = random_state.choice(n_samples, n_samples, replace=False)

                # Re-order each variable according to the sorting index.
                for s in samples:
                    s[:] = s[i_sort]

        return tuple(samples)

    def _compute_sub_population_observables(
            self,
            observables: Observables,
        ) -> typing.List[Observables]:
        return [
            tuple(observables[i] for i in indices)
            for indices in self._sub_population_observables_indices
        ]

    def _compute_sub_population_observables_indices(self) -> None:
        self._sub_population_observables_indices = [
            [self.coord_system.index(coord) for coord in pop.coord_system]
            for pop in self.sub_populations
        ]

    def _compute_sub_population_parameter_name_pairs(
            self,
            parameters: Parameters,
        ) -> None:
        self._sub_population_parameter_name_pairs = [] # type: typing.List[typing.List[typing.Tuple[str,str]]]

        for suffix in self.suffixes:
            param_name_pairs = [] # type: typing.List[typing.Tuple[str,str]]

            for suffixed_name, value in parameters.items():
                if suffixed_name.endswith(suffix):
                    clean_name = suffixed_name[:-len(suffix)]
                    param_name_pairs.append((suffixed_name, clean_name))

            self._sub_population_parameter_name_pairs.append(param_name_pairs)

    def _compute_sub_population_parameters(
            self,
            parameters: Parameters,
        ) -> typing.List[Parameters]:
        if not self._sub_population_parameter_name_pairs_initialized:
            self._compute_sub_population_parameter_name_pairs(parameters)
            self._sub_population_parameter_name_pairs_initialized = True

        return [
            {
                clean_name : parameters[suffixed_name]
                for suffixed_name, clean_name in param_name_pairs
            }
            for param_name_pairs in self._sub_population_parameter_name_pairs
        ]

    def _check_coord_systems_covered(self) -> None:
        """
        Ensures all sub-population coordinate systems are encompassed in the
        overall population's coordinate system.

        TODO: Needs to be able to handle transformed coordinates.
        """
        for pop in self.sub_populations:
            if not self._coord_system_set.issuperset(pop.coord_system):
                raise ValueError(
                    "Sub-population '{}' is in coordinates '{}', which are not "
                    "contained in the overall population's coordinates '{}'."
                    .format(pop, pop.coord_system, self.coord_system)
                )

    def params_valid(self, parameters: Parameters) -> WhereType:
        from functools import reduce

        params_shape = numpy.shape(next(iter(parameters.values())))

        is_valid = numpy.ones(params_shape, dtype=bool)

        sub_population_parameters = (
            self._compute_sub_population_parameters(parameters)
        )

        iterables = zip(
            sub_population_parameters, self.sub_populations, self.suffixes,
        )
        for parameters_subpop, subpop, suffix in iterables:
            subpop_valid = subpop.params_valid(parameters_subpop)
            is_valid &= subpop_valid

            debug_verbose(
                "Subpopulation", suffix, "is valid here:", subpop_valid,
                mode="mixture_pop", flush=True,
            )

        return is_valid

    def get_subpops(
            self, suffixes: typing.Sequence[str],
        ) -> "MixturePopulation":
        """
        Get a subset of the mixture's sub-populations.

        Provide a list of sub-population suffixes, and output will be a new
        :class:`MixturePopulation` with just those sub-populations.
        """
        suffix_to_population = {
            suffix : self.sub_populations[self.suffixes.index(suffix)]
            for suffix in suffixes
        }
        return MixturePopulation(
            self.coord_system, suffix_to_population,
            transformations=self.transformations,
        )


    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> Population:
        # Handle trivial case.
        if coord_system == self.coord_system:
            debug_verbose(
                "Undergoing trivial coordinate change.",
                mode="mixture_pop", flush=True,
            )
            return self

        try:
            debug_verbose(
                "Attempting coordinate change in each sub-population.",
                mode="mixture_pop", flush=True,
            )
            new_suffix_to_population = {
                suffix : population.to_coords(
                    coord_system, use_transformations=use_transformations,
                )
                for suffix, population in zip(self.suffixes, self.sub_populations)
            }

            pop = MixturePopulation(
                coord_system,
                new_suffix_to_population,
                transformations=self.transformations,
            )

            debug_verbose(
                "Coordinate change in each sub-population succeeded:",
                new_suffix_to_population,
                mode="mixture_pop", flush=True,
            )

            return pop
        except ValueError as e:
            debug_verbose(
                "Coordinate change in each sub-population failed:",
                e,
                mode="mixture_pop", flush=True,
            )

        return super().to_coords(
            coord_system, use_transformations=use_transformations,
        )



class ConditionalPopulation(Population):
    """
    Defines a population over (possibly multi-dimensional each) variables
    :math:`x` and :math:`y`, by implementing methods for the conditional
    probability :math:`p(x | y)`, and taking as input another
    :py:class:`Population` object which implements :math:`p(y)`.

    Implementing classes must override methods :py:meth:`cond_pdf` and
    :py:meth:`cond_rvs`, and the methods :py:meth:`pdf` and :py:meth:`rvs` will
    automatically produce the joint distribution.

    If one would like to access the :py:meth:`cond_pdf` or :py:meth:`cond_rvs`
    methods directly, without a specific :math:`p(y)` in mind, one can set
    ``pop_y=None`` to indicate this.
    """
    def __init__(
            self,
            pop_y: typing.Optional[Population],
            coord_system_x: CoordinateSystem, coord_system_y: CoordinateSystem,
            param_names_x: typing.Tuple[str,...],
        ) -> None:
        if pop_y is None:
            coord_system = coord_system_x
            param_names = param_names_x
            transformations = transformations_nil
        else:
            coord_system = coord_system_x + pop_y.coord_system
            param_names_list = list(pop_y.param_names)
            for name in param_names_x:
                if name not in param_names_list:
                    param_names_list.append(name)
            param_names = tuple(param_names_list)
            transformations = pop_y.transformations

        ## TODO: need to be able to specify what depends on what precisely.
        ## Will need to be able to handle transformations as well.
        warnings.warn(
            "ConditionalPopulation does not yet support determination of "
            "which derived coordinates are correlated with each other.  As a "
            "temporary workaround, all x-coordinates are assumed to be "
            "correlated with all y-coordinates, and all y-coordinates are "
            "assumed to be un-correlated.  Be careful when using "
            "'to_reordered_coords' method."
        )
        correlated_coords = frozenset(
            frozenset.union(*(
                frozenset({coord_x, coord_y}) for coord_y in pop_y.coord_system
            ))
            for coord_x in coord_system_x
        )

        super().__init__(
            coord_system, param_names,
            transformations=transformations,
            correlated_coords=correlated_coords,
        )

        self.pop_y = pop_y
        self.coord_system_x = coord_system_x
        self.coord_system_y = coord_system_y

        self._param_names_x = param_names_x

        # Determine how to transform from `pop_y`'s CoordinateSystem to what is
        # actually needed from it.
        if pop_y is None:
            self.transf_pop_to_y = lambda obs: None
        else:
            transf_pop_to_y = transformations[
                pop_y.coord_system, coord_system_y,
            ]
            self.transf_pop_to_y = transf_pop_to_y

    @property
    def param_names_x(self):
        return self._param_names_x

    def normalization(
            self,
            parameters: Parameters, where: WhereType=True,
            **kwargs
        ) -> Numeric:
        if self.pop_y is None:
            raise RuntimeError(
                "Must define `pop_y` in constructor."
            )

        return self.pop_y.normalization(parameters, where=where, **kwargs)

    def cond_pdf(
            self,
            observables_x: Observables, observables_y: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:
        """
        This method should implement the PDF for :math:`p(x | y)` with the given
        set of population parameters.  The observables for :math:`x` and
        :math:`y` will be passed in automatically.
        """
        raise NotImplementedError

    def cond_rvs(
            self,
            observables_y: Observables,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> Observables:
        """
        This method should implement the random sampler for :math:`p(x | y)`
        with the given set of population parameters.  The observables for
        :math:`y` and will be passed in automatically, and this should return
        one sample for each value of :math:`y`, and for each value of the
        population parameters.
        """
        raise NotImplementedError

    def marg_pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:
        return self.pop_y.pdf(
            observables, parameters,
            where=where,
        )

    def marg_intensity(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:
        return self.pop_y.intensity(
            observables, parameters,
            where=where,
        )

    def marg_rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        observables_pop = self.pop_y.rvs(
            n_samples, parameters,
            where=where, random_state=random_state,
            **kwargs
        )
        # Convert to expected coordinates.
        observables_y = self.transf_pop_to_y(observables_pop)

        return observables_y


    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        if self.pop_y is None:
            raise RuntimeError(
                "Must define `pop_y` in constructor."
            )

        # Parse out the subset of observables that correspond to the conditional
        # dimensions ('x'), and to the given dimensions ('y', after
        # transforming).
        observables_x = observables[:len(self.coord_system_x)]
        observables_pop = observables[len(self.coord_system_x):]
        observables_y = self.transf_pop_to_y(observables_pop)

        # Compute p(y).
        p_of_y = self.pop_y.pdf(
            observables_pop, parameters,
            where=where,
            **kwargs
        )
        # Compute p(x | y).

        p_of_x_given_y = self.cond_pdf(
            observables_x, observables_y,
            parameters,
            where=where,
        )
        # Return p(x, y) = p(x | y) p(y).
        return p_of_y * p_of_x_given_y

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        if self.pop_y is None:
            raise RuntimeError(
                "Must define `pop_y` in constructor."
            )

        # Draw random samples from p(y).
        observables_pop = self.pop_y.rvs(
            n_samples, parameters,
            where=where, random_state=random_state,
            **kwargs
        )
        # Convert to expected coordinates.
        observables_y = self.transf_pop_to_y(observables_pop)
        # Draw random samples from p(x | y).
        observables_x = self.cond_rvs(
            observables_y,
            parameters,
            where=where,
            random_state=random_state
        )
        # Concatenate samples from the two distributions, to get samples from
        # p(x, y).
        return observables_x + observables_pop

    def params_valid(self, parameters: Parameters) -> WhereType:
        """
        Determines which parameters are valid for this conditional population,
        looking at the y-population as well as this object's
        ``cond_params_valid``.  One should override ``cond_params_valid`` if any
        custom conditions are needed.
        """
        return (
            self.cond_params_valid(parameters) &
            self.pop_y.params_valid(parameters)
        )

    def cond_params_valid(self, parameters: Parameters) -> WhereType:
        params_shape = numpy.shape(next(iter(parameters.values())))

        return numpy.broadcast_to(True, params_shape)


    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> Population:
        if self.pop_y is None:
            raise RuntimeError(
                "Must define `pop_y` in constructor."
            )

        # Attempt to convert from `pop_y`
        try:
            return self.pop_y.to_coords(
                coord_system, use_transformations=use_transformations,
            )
        except ValueError:
            pass

        return super().to_coords(
            coord_system,
            use_transformations=use_transformations,
        )

    def to_reordered_coords(
            self,
            coord_system_x: CoordinateSystem,
            coord_system_pop: CoordinateSystem,
            use_transformations: bool=False,
        ) -> "ConditionalPopulation":
        """
        Returns a new ``ConditionalPopulation`` with the marginalized and
        conditional variables reordered.
        """
        # Currently don't support ``use_transformations`` for this.
        if use_transformations:
            raise NotImplementedError(
                "Transformations currently unsupported by 'to_reordered_coords'"
            )

        ## Assert that everything in self.coord_system_x is in coord_system_x
        ## Assert that everything in self.coord_system_y makes up the remainder
        ## of both coord_system_x and coord_system_y, with no repetitions.

        # Verify that ``self.coord_system_x`` is entirely contained within
        # ``coord_system_x``, and that ``self.pop_y.coord_system`` makes up the
        # rest of the coordinates in both ``coord_system_x`` and
        # ``coord_system_pop``.  We do this by removing elements from sets of
        # coordinates, which should be empty at the end.
        coords_in_x = list(coord_system_x)
        coords_in_pop = set(coord_system_pop)

        missing_coords_from_x = set()
        missing_coords_from_pop = set()

        for coord in self.coord_system_x:
            if coord in coords_in_x:
                coords_in_x.remove(coord)
            else:
                missing_coords_from_x.add(coord)

        # Store remaining coordinates in x for later use if we don't crash.
        coord_system_x_carry_over = CoordinateSystem(*coords_in_x)

        for coord in self.pop_y.coord_system:
            if coord in coords_in_x:
                coords_in_x.remove(coord)
            elif coord in coords_in_pop:
                coords_in_pop.remove(coord)
            else:
                missing_coords_from_pop.add(coord)

        error_msg = []
        if len(missing_coords_from_x) != 0:
            error_msg.append(
                "    'coord_system_x' is missing coordinates from "
                "'self.coord_system_x': {}"
                .format(
                    ", ".join(coord.name for coord in missing_coords_from_x)
                )
            )
        if len(missing_coords_from_pop) != 0:
            error_msg.append(
                "    'coord_system_x' and 'coord_system_pop' are missing "
                "coordinates from 'self.pop_y.coord_system': {}"
                .format(
                    ", ".join(coord.name for coord in missing_coords_from_pop)
                )
            )
        if len(coords_in_x) != 0:
            error_msg.append(
                "    'coord_system_x' has unattainable coordinates: {}"
                .format(", ".join(coord.name for coord in coords_in_x))
            )
        if len(coords_in_pop) != 0:
            error_msg.append(
                "    'coord_system_pop' has unattainable coordinates: {}"
                .format(", ".join(coord.name for coord in coords_in_pop))
            )

        # Also verify that we can still obtain ``self.coord_system_y`` from
        # ``coord_system_pop`` by using ``self.transformations``.
        try:
            self.transformations[coord_system_pop, self.coord_system_y]
        except KeyError:
            error_msg.append(
                "    'self.coord_system_y' is not obtainable by "
                "transformations from 'coord_system_pop'."
            )

        # Produce error message if needed.
        if len(error_msg) != 0:
            msg = (
                "The following errors occurred when transforming to a "
                "reordered 'ConditionalPopulation':\n{}"
            ).format("\n".join(error_msg))
            raise ValueError(msg)

        ## Perform reordering ##

        # Trivial reordering, return self.
        is_trivial = (
            (coord_system_x == self.coord_system_x) and
            (coord_system_pop == self.pop_y.coord_system)
        )
        if is_trivial:
            return self

        # TODO: add simple re-ordering where the x/y split is unchanged.  For
        # now we let this special case be handled by the general case below.

        # Non-trivial reordering.  Split ``pop_y`` into the part that now goes
        # into ``coord_system_x``, and the remainder.
        pop_x_carry_over = self.pop_y.to_coords(
            coord_system_x_carry_over, use_transformations=False,
        )
        pop_y = self.pop_y.to_coords(
            coord_system_pop, use_transformations=False,
        )

        return ReorderedConditionalPopulation(
            self, pop_x_carry_over, pop_y,
            coord_system_x,
        )




class ReorderedConditionalPopulation(ConditionalPopulation):
    def __init__(
            self,
            pop_cond: ConditionalPopulation,
            pop_x_carry_over: Population, pop_y: Population,
            coord_system_x: CoordinateSystem,
        ) -> None:
        param_names_x_list = list(pop_cond.param_names_x)
        for param_name in pop_x_carry_over.param_names:
            if param_name not in param_names_x_list:
                param_names_x_list.append(param_name)
        param_names_x = tuple(param_names_x_list)

        super().__init__(
            pop_y,
            coord_system_x, pop_cond.coord_system_y,
            param_names_x,
        )

        self._pop_cond = pop_cond
        self._pop_x_carry_over = pop_x_carry_over

    def cond_pdf(
            self,
            observables_x: Observables, observables_y: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:
        """
        Implements the PDF for :math:`p(x | y)` with the given set of population
        parameters.
        """
        observables_x_orig = tuple(
            observables_x[self.coord_system_x.index(coord)]
            for coord in self._pop_cond.coord_system_x
        )
        observables_x_carry_over = tuple(
            observables_x[self.coord_system_x.index(coord)]
            for coord in self._pop_x_carry_over.coord_system
        )

        p_x_given_y = self._pop_cond.cond_pdf(
            observables_x_orig, observables_y, parameters,
            where=where,
        )
        p_x_given_y *= self._pop_x_carry_over.pdf(
            observables_x_carry_over, parameters,
            where=where,
        )

        return p_x_given_y


    def cond_rvs(
            self,
            observables_y: Observables,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> Observables:
        """
        Implements the random sampler for :math:`p(x | y)` with the given set of
        population parameters.
        """
        observables_x_orig = self._pop_cond.cond_rvs(
            observables_y, parameters,
            where=where, random_state=random_state,
        )

        # Draw random samples from p(y).  Will have extra single-element
        # dimension at the end, which we remove
        observables_x_carry_over = self._pop_x_carry_over.rvs(
            1, parameters,
            where=where, random_state=random_state,
        )
        observables_x_carry_over = tuple(
            obs[...,0] for obs in observables_x_carry_over
        )

        observables_x_list = []
        for coord in self.coord_system_x:
            if coord in self._pop_cond.coord_system_x:
                i = self._pop_cond.coord_system_x.index(coord)
                observables_x_list.append(observables_x_orig[i])
            else:
                i = self._pop_x_carry_over.coord_system.index(coord)
                observables_x_list.append(observables_x_carry_over[i])

        return tuple(observables_x_list)

    def to_reordered_coords(
            self,
            coord_system_x: CoordinateSystem,
            coord_system_pop: CoordinateSystem,
            use_transformations: bool=False,
        ) -> ConditionalPopulation:
        return self._pop_cond.to_reordered_coords(
            coord_system_x, coord_system_pop,
            use_transformations=use_transformations,
        )


class TruncatedPopulation(Population):
    def __init__(
            self,
            base_population: Population,
            coord_system_trunc: CoordinateSystem,
            param_names_trunc: typing.Tuple[str,...],
        ) -> None:
        correlated_coords = base_population.correlated_coords.union(
            frozenset({frozenset(coord_system_trunc)})
        )
        super().__init__(
            base_population.coord_system,
            base_population.param_names+param_names_trunc,
            transformations=base_population.transformations,
            correlated_coords=correlated_coords,
        )
        self._base_population = base_population

        self._coord_system_trunc = coord_system_trunc
        self._param_names_trunc = param_names_trunc
        # Determine the indices of observables to extract for truncation, to
        # save time later.
        self._obs_trunc_indices = [
            self.coord_system.index(coord) for coord in self.coord_system_trunc
        ]


    @property
    def base_population(self):
        return self._base_population

    @property
    def coord_system_trunc(self):
        return self._coord_system_trunc

    @property
    def param_names_trunc(self):
        return self._param_names_trunc

    def reject_samples(
            self,
            observables_trunc: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> WhereType:
        r"""
        Returns ``True`` for any rejected observables, and ``False`` for any
        that are acceptable.  Only uses the subset of coordinates specified by
        ``coord_system_trunc`` attribute, which defaults to the full coordinate
        system if not provided.

        Must be implemented in a sub-class.
        """
        raise NotImplementedError

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        # Determine shapes
        shape_obs = observables[0].shape
        shape_params = next(iter(parameters.values())).shape
        shape_out = shape_params + shape_obs

        # Ensure ``where`` can be used as an array.
        where_arr = numpy.broadcast_to(where, shape_params)

        # Compute the PDF wherever it has support.
        pdf_out = numpy.zeros(shape_out)

        # Pull out observables which contribute to truncation
        observables_trunc = tuple(
            observables[i] for i in self._obs_trunc_indices
        )

        for idx in numpy.ndindex(*shape_params):
            if not where_arr[idx]:
                continue

            parameters_at_idx = {
                name : numpy.asarray(values[idx])
                for name, values in parameters.items()
            }
            i_no_support = self.reject_samples(
                observables_trunc, parameters_at_idx,
            )
            i_support = numpy.invert(i_no_support, out=i_no_support)

            observables_support = tuple(obs[i_support] for obs in observables)

            pdf_out[idx][i_support] = self.base_population.pdf(
                observables_support, parameters_at_idx,
                **kwargs
            )

        # Determine the relative normalization by counting the fraction of
        # samples from the base population which will be accepted.
        n_samples = 2048
        cand_observables = self.base_population.rvs(
            n_samples, parameters,
            where=where,
            **kwargs
        )
        cand_observables_trunc = tuple(
            cand_observables[i] for i in self._obs_trunc_indices
        )
        i_reject = self.reject_samples(
            cand_observables_trunc, parameters,
            where=where,
        )
        i_accept = numpy.invert(i_reject, out=i_reject)
        n_accept = numpy.count_nonzero(i_accept, axis=-1)

        relative_norm = (
            (n_samples/n_accept)[(...,) + (None,)*len(shape_obs)]
        )
        pdf_out *= relative_norm

        return pdf_out

    def intensity(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        # Determine shapes
        shape_obs = observables[0].shape
        shape_params = next(iter(parameters.values())).shape
        shape_out = shape_params + shape_obs

        # Ensure ``where`` can be used as an array.
        where_arr = numpy.broadcast_to(where, shape_params)

        # Compute the INTENSITY wherever it has support.
        intensity_out = numpy.zeros(shape_out)

        # Pull out observables which contribute to truncation
        observables_trunc = tuple(
            observables[i] for i in self._obs_trunc_indices
        )

        for idx in numpy.ndindex(*shape_params):
            if not where_arr[idx]:
                continue

            parameters_at_idx = {
                name : numpy.asarray(values[idx])
                for name, values in parameters.items()
            }
            i_no_support = self.reject_samples(
                observables_trunc, parameters_at_idx,
            )
            i_support = numpy.invert(i_no_support, out=i_no_support)

            observables_support = tuple(obs[i_support] for obs in observables)

            intensity_out[idx][i_support] = self.base_population.intensity(
                observables_support, parameters_at_idx,
                **kwargs
            )

        # Determine the relative normalization by counting the fraction of
        # samples from the base population which will be accepted.
        n_samples = 2048
        cand_observables = self.base_population.rvs(
            n_samples, parameters,
            where=where,
            **kwargs
        )
        cand_observables_trunc = tuple(
            cand_observables[i] for i in self._obs_trunc_indices
        )
        i_reject = self.reject_samples(
            cand_observables_trunc, parameters,
            where=where,
        )
        i_accept = numpy.invert(i_reject, out=i_reject)
        n_accept = numpy.count_nonzero(i_accept, axis=-1)

        relative_norm = (
            (n_samples/n_accept)[(...,) + (None,)*len(shape_obs)]
        )
        intensity_out *= relative_norm

        return intensity_out

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        params_shape = numpy.shape(next(iter(parameters.values())))


        parameters_arr = {
            name : numpy.asarray(values)
            for name, values in parameters.items()
        }
        where_arr = numpy.broadcast_to(where, params_shape)

        base_samples = self.base_population.rvs(
            n_samples, parameters,
            where=where, random_state=random_state,
            **kwargs
        )

        n_observables = len(base_samples)

        for i in numpy.ndindex(*params_shape):
            # Leave point un-initialized if exluded by `where`.
            if not where_arr[i]:
                continue

            parameters_at_i = {
                param_name : values[i]
                for param_name, values in parameters_arr.items()
            }

            while True:
                ## TODO: Avoid validating samples that have already been
                ## validated.  Should just update `i_invalid` in-place.

                # Extract subset of samples that correspond to the parameters
                # at index `i`, only including those that contribute to
                # truncation.
                base_samples_trunc_at_i = tuple(
                    base_samples[j][i] for j in self._obs_trunc_indices
                )

                # Determine which samples violate the rejection criteria, and
                # count how many there are.
                i_invalid = self.reject_samples(
                    base_samples_trunc_at_i, parameters_at_i,
                )
                n_rejected = numpy.count_nonzero(i_invalid)

                # If no samples violate the criteria, we've succeeded and can
                # move on to the next parameter realization.
                if n_rejected == 0:
                    break

                # Draw new samples to replace all of the rejected samples.  Some
                # of these may be rejected still, so they will be inspected on
                # the next iteration of this loop.
                new_samples_at_i = self.base_population.rvs(
                    n_rejected, parameters_at_i,
                    where=True, random_state=random_state,
                    **kwargs
                )

                # Overwrite rejected base samples with new samples.
                for k in range(n_observables):
                    base_samples[k][i][i_invalid] = new_samples_at_i[k]

        ## TODO: Reorder to match coordinate system's ordering.
        return base_samples

    def params_valid(self, parameters: Parameters) -> WhereType:
        """
        Determines which parameters are valid for this truncated population,
        looking at the base as well as this object's
        ``trunc_params_valid``.  One should override ``trunc_params_valid`` if
        any custom conditions are needed.
        """
        return (
            self.trunc_params_valid(parameters) &
            self.base_population.params_valid(parameters)
        )

    def trunc_params_valid(self, parameters: Parameters) -> WhereType:
        params_shape = numpy.shape(next(iter(parameters.values())))

        return numpy.broadcast_to(True, params_shape)


    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return self.base_population.normalization(
            parameters,
            where=where, **kwargs
        )

    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> Population:
        from itertools import chain, product
        ## HACK: shouldn't need this import
        from inspect import getfullargspec

        # Handle trivial case.
        if coord_system == self.coord_system:
            return self

        # If the target coordinate system includes everything needed for
        # truncation, and those coordinates are not correlated with anything
        # being dropped, transform the base population into the appropriate
        # coordinate system, and truncate it.
        new_system_is_subset = coord_system.issubset(self.coord_system)
        trunc_coords_included = self.coord_system_trunc.issubset(coord_system)
        if new_system_is_subset and trunc_coords_included:
            dropped_coords_uncorrelated = True

            dropped_coords = set(self.coord_system) - set(coord_system)
            iterables = product(
                coord_system, dropped_coords,
                self.correlated_coords,
            )
            for kept_coord, dropped_coord, correlated_coords in iterables:
                # If we find one pair of (kept, dropped) coordinates that are
                # correlated, we can't do this case.
                if {kept_coord, dropped_coord}.issubset(correlated_coords):
                    dropped_coords_uncorrelated = False
                    break

            # If all our tests passed, then perform coordinate transformation on
            # the base population, and then truncate the transformed value.
            if dropped_coords_uncorrelated:
                new_base_population = (
                    self.base_population.to_coords(coord_system)
                )
                ## HACK: Inspect the constructor for ``type(self)``, hope it has
                ## only named arguments, and hope they are all explicitly
                ## defined as attributes which can be obtained with ``getattr``.
                ## Replace ``base_population`` with ``new_base_population``, and
                ## pass everything to said constructor.
                constructor_spec = getfullargspec(type(self).__init__)

                has_unsupported_args = (
                    (constructor_spec.varargs is not None) or
                    (constructor_spec.varkw is not None)
                )
                if has_unsupported_args:
                    warnings.warn(
                        "Cannot use efficient truncated population "
                        "transformation here, as it currently relies on manual "
                        "inspection of the object's constructor, and cannot "
                        "handle '*args' and '**kwargs' in the constructor."
                    )
                else:
                    missing_args = set()
                    kwargs = {"base_population": new_base_population}
                    iterables = chain(
                        constructor_spec.args, constructor_spec.kwonlyargs,
                    )
                    for name in iterables:
                        if name not in {"self", "base_population"}:
                            if hasattr(self, name):
                                kwargs[name] = getattr(self, name)
                            else:
                                missing_args.add(name)

                    if len(missing_args) != 0:
                        warnings.warn(
                            "Cannot use efficient truncated population "
                            "transformation here, as it currently relies on "
                            "manual inspection of the object's constructor, "
                            "and has failed to find the arguments it needs in "
                            "the old object's constructor."
                        )
                    else:
                        return type(self)(**kwargs)

        # If the truncated coordinates have been completely dropped, and they
        # are un-correlated to the remaining coordinates, simply transform the
        # base population, with no truncation involved.
        trunc_coords_dropped = (
            len(set(self.coord_system_trunc) & set(coord_system)) == 0
        )
        if new_system_is_subset and trunc_coords_dropped:
            trunc_coords_uncorrelated = True

            dropped_coords = set(self.coord_system) - set(coord_system)
            iterables = product(
                coord_system, self.coord_system_trunc,
                self.correlated_coords,
            )
            for kept_coord, trunc_coord, correlated_coords in iterables:
                # If we find one pair of (kept, trunc) coordinates that are
                # correlated, we can't do this case.
                if {kept_coord, trunc_coord}.issubset(correlated_coords):
                    trunc_coords_uncorrelated = False
                    break

            # If all our tests passed, then perform coordinate transformation on
            # the base population, and return that.
            if trunc_coords_uncorrelated:
                return self.base_population.to_coords(coord_system)

        # Fall back to basic coordinate transformations.
        return super().to_coords(
            coord_system,
            use_transformations=use_transformations,
        )
