from __future__ import division, print_function

_header = "# "
_header += "\t".join([
    "Parameter",
    "MedianValue",
    "LowerValue",
    "UpperValue",
    "LowerDelta",
    "UpperDelta",
])


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "ppd_samples",
        help="Samples from the PPD.",
    )

    parser.add_argument(
        "output_pop_params",
        help="File to output population parameter credible region values to.",
    )
    parser.add_argument(
        "output_indiv_params",
        help="File to output population individual credible region values to.",
    )

    parser.add_argument(
        "--confidence-level",
        type=float, default=0.90,
        help="Total probability to inclose between upper and lower limits "
             "(default is 0.90).",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import h5py
    import numpy

    from . import prob
    from . import utils
    from .. import credible_regions

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    quantiles = [
        0.5 * (1 - args.confidence_level),
        0.5,
        0.5 * (1 + args.confidence_level),
    ]


    def write_line(file, param_name, samples, weights=None):
        lower_val, median_val, upper_val = credible_regions.cr(
            quantiles, samples, weights=weights,
        )

        lower_delta = median_val - lower_val
        upper_delta = upper_val - median_val

        print(
            param_name,
            median_val,
            lower_val, upper_val,
            lower_delta, upper_delta,
            sep="\t",
            file=file,
        )


    with h5py.File(args.posteriors, "r") as post_file:
        M_max = post_file.attrs["M_max"]
        iid_spins = post_file.attrs["iid_spins"]

        param_names = prob.param_names

        posteriors = post_file["pos"].value
        n_samples = len(posteriors)

        constants = dict(post_file["constants"].attrs)
        duplicates = dict(post_file["duplicates"].attrs)

        posteriors_dict = dict(zip(
            param_names,
            utils.get_params(
                posteriors, constants, duplicates, param_names,
            )
        ))

        def is_variable(param_name):
            if param_name in constants:
                return False
            if param_name in duplicates:
                return False

            return True

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(
                numpy.power(10.0, posteriors_dict["log10_rate"])
            )
            if numpy.isscalar(weights):
                weights = numpy.tile(weights, n_samples)
        else:
            weights = None

        with open(args.output_pop_params, "w") as out_file:
            print(_header, file=out_file)

            for param_name in param_names:
                if not is_variable(param_name):
                    continue

                write_line(
                    out_file, param_name,
                    posteriors_dict[param_name], weights,
                )

        ppd_samples = numpy.genfromtxt(args.ppd_samples, names=True)

        m1_source = ppd_samples["m1_source"]
        m2_source = ppd_samples["m2_source"]

        a1 = ppd_samples["a1"]
        a2 = ppd_samples["a2"]

        costilt1 = ppd_samples["costilt1"]
        costilt2 = ppd_samples["costilt2"]

        m_source = numpy.concatenate((m1_source, m2_source))
        M_source = m1_source + m2_source
        q = m2_source / m1_source

        a = numpy.concatenate((a1, a2))
        costilt = numpy.concatenate((costilt1, costilt2))

        chi_eff = (m1_source*a1*costilt1 + m2_source*a2*costilt2) / M_source


        with open(args.output_indiv_params, "w") as out_file:
            print(_header, file=out_file)

            for param_name in ppd_samples.dtype.names:
                write_line(
                    out_file, param_name,
                    ppd_samples[param_name],
                )

            write_line(
                out_file, "m_source",
                m_source,
            )
            write_line(
                out_file, "M_source",
                M_source,
            )
            write_line(
                out_file, "q",
                q,
            )
            write_line(
                out_file, "a",
                a,
            )
            write_line(
                out_file, "costilt",
                costilt,
            )
            write_line(
                out_file, "chi_eff",
                chi_eff,
            )
