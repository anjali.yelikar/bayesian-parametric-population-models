r"""
This module defines all of the probability density functions and random sampling
functions for the power law mass distribution + beta distribution spin magnitude
+ truncated Gaussian tilt distribution model.
"""

from __future__ import division, print_function

# Names of all (possibly free) parameters of this model, in the order they
# should appear in any array of samples (e.g., MCMC posterior samples).
param_names = [
    "log10_rate",
    "alpha_m", "beta_q", "m_min", "m_max",
    "E_chi1", "Var_chi1", "mu_cos1", "sigma_cos1",
    "E_chi2", "Var_chi2", "mu_cos2", "sigma_cos2",
]
# Number of (possibly free) parameters for this population model.
ndim_pop = len(param_names)


def spin_mu_sigma_sq_2_alpha_beta(mu, sigma_sq):
    import numpy

    mu_sq = numpy.square(mu)

    alpha = (mu_sq - mu*mu_sq - mu*sigma_sq) / sigma_sq
    beta = (mu-1) * (sigma_sq + mu_sq - mu) / sigma_sq

    return alpha, beta


def spin_alpha_beta_2_mu_sigma_sq(alpha, beta):
    import numpy

    a_plus_b = alpha + beta

    mu = alpha / a_plus_b
    sigma_sq = alpha*beta / (numpy.square(a_plus_b) * (a_plus_b + 1))

    return mu, sigma_sq


def tilt_model(mu, sigma):
    r"""
    Creates a ``scipy.stats.rv_continuous`` object represented a Gaussian
    centered at 1.0 with a standard deviation of ``sigma``, which is then
    truncated to the range [-1.0, +1.0], representing the range of possible
    :math:`\cos(\theta)` values allowed for an object's spin vector.
    """
    import scipy.stats

    return scipy.stats.truncnorm(
        (-1.0 - mu) / sigma, (+1.0 - mu) / sigma,
        loc=mu, scale=sigma,
    )


def powerlaw_rvs(*args, **kwargs):
    from ..powerlaw.prob import powerlaw_rvs
    return powerlaw_rvs(*args, **kwargs)

# def powerlaw_rvs(N, alpha, x_min, x_max, rand_state=None):
#     r"""
#     Draws ``N`` samples from a power law :math:`p(x) \propto x^{-\alpha}`, with
#     support only betwen ``x_min`` and ``x_max``. Uses inverse transform
#     sampling, drawing from the function

#     .. math::
#        \left((1-U) L + U H\right)^{1/\beta}

#     where :math:`U` is uniformly distributed on (0, 1), and

#     .. math::
#        \beta = 1 - \alpha,
#        L = x_{\mathrm{min}}^\beta,
#        H = x_{\mathrm{max}}^\beta

#     :param int N: Number of random samples to draw.

#     :param float alpha: Power law index on :math:`p(x) \propto x^{-\alpha}`.

#     :param float x_min: Lower limit of power law.

#     :param float x_max: Upper limit of power law.

#     :param numpy.random.RandomState rand_state: (optional) State for RNG.

#     :return: array_like, shape (N,)
#         Array of random samples drawn from distribution.
#     """
#     import numpy
#     from ..utils import check_random_state

#     # Upgrade ``rand_state`` to an actual ``numpy.random.RandomState`` object,
#     # if it isn't one already.
#     rand_state = check_random_state(rand_state)

#     # Power law index ``alpha`` only appears as ``1 - alpha`` in the equations,
#     # so define this quantity as ``beta`` and use it henceforth.
#     beta = 1 - alpha

#     # Uniform random samples between zero and one.
#     U = rand_state.uniform(size=N)

#     # x_{min,max}^beta, which appear in the inverse transform equation
#     L = numpy.power(x_min, beta)
#     H = numpy.power(x_max, beta)

#     # Compute the random samples.
#     return numpy.power((1-U)*L + U*H, 1.0/beta)



def marginal_m1_pdf_noMmax(
        m_1,
        alpha_m, m_min, m_max,
        where=True, out=None,
    ):
    import numpy

    # Ensure everything is a numpy array.
    m_1 = numpy.asarray(m_1)
    alpha_m = numpy.asarray(alpha_m)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    alpha_m, m_min, m_max = numpy.broadcast_arrays(alpha_m, m_min, m_max)

    # Determine the shapes of the parameters and hyperparameters.
    S = m_1.shape
    T = alpha_m.shape
    # Compute the output shape, as well as the shape of its transpose.
    TS = T+S
    TS_T = S[::-1]+T[::-1]

    # Broadcast `where` to the output shape.
    where_TS = numpy.broadcast_to(where.T, TS_T).T

    # Initialize the output array if not provided.
    out_type = m_1.dtype
    if out is None:
        pdf = numpy.zeros(TS, dtype=out_type)
    else:
        pdf = out
        pdf[where_TS] = 0.0

    # Create temporary arrays to re-use.
    tmp_TS = numpy.empty_like(pdf)
    tmp_i_T = numpy.empty(T, dtype=bool)

    # Determine where the support is so we can evaluate it there.
    i_eval_tmp = numpy.zeros(TS, dtype=bool)
    i_big_enough = numpy.less_equal.outer(
        m_min, m_1,
        where=where_TS, out=i_eval_tmp,
    )
    i_small_enough = numpy.greater_equal.outer(
        m_max, m_1,
        where=where_TS,
    )
    i_eval = numpy.logical_and(
        i_big_enough, i_small_enough,
        where=where_TS,
        out=i_eval_tmp,
    )


    # Compute the normalizing constant D.
    Dinv = numpy.empty_like(pdf)

    # Handle the special case of alpha_m == 1 separately.
    i_one_base = numpy.equal(alpha_m, 1.0, out=tmp_i_T)
    i_one = numpy.broadcast_to(i_one_base, TS_T).T

    # Store ln(m_max) in D^-1 where it appears.
    ln_mmax = numpy.log(
        numpy.broadcast_to(m_max.T, TS_T).T,
        where=i_one, out=Dinv,
    )

    # Store ln(m_min) in tmp_TS, then subtract it from D^-1 where it appears.
    ln_mmin = numpy.log(
        numpy.broadcast_to(m_min.T, TS_T).T,
        where=i_one, out=tmp_TS,
    )
    numpy.subtract(ln_mmax, ln_mmin, where=i_one, out=Dinv)

    # Handle the remaining non-special cases of betaalpha != 1.
    i_nonspecial_base = numpy.invert(i_one_base, out=tmp_i_T)
    i_nonspecial = numpy.broadcast_to(i_nonspecial_base, TS_T).T

    # Compute (1-alpha_m).T as we will use this multiple times
    one_minus_alpha_T = numpy.subtract(1.0, alpha_m, where=where).T

    # Store m_max^(1-alpha) in D^-1 where it appears.
    m_max_to_the_one_minus_alpha = numpy.power(
        m_max.T, one_minus_alpha_T,
        where=i_nonspecial.T, out=Dinv.T,
    ).T

    # Store m_min^(1-alpha) in tmp_TS, then subtract it from D^-1 where it
    # appears.
    m_min_to_the_one_minus_alpha = numpy.power(
        m_min.T, one_minus_alpha_T,
        where=i_nonspecial.T, out=tmp_TS.T,
    ).T
    m_to_powers_difference = numpy.subtract(
        m_max_to_the_one_minus_alpha, m_min_to_the_one_minus_alpha,
        where=i_nonspecial, out=Dinv,
    )

    # Put in the 1/(1-alpha) term in D^-1 and now it's complete.
    numpy.divide(
        Dinv.T, one_minus_alpha_T,
        where=i_nonspecial.T, out=Dinv.T,
    )

    # Compute the m_1 term, and store it in the pdf output.
    numpy.power.outer(
        m_1.T, -alpha_m.T,
        where=i_eval.T, out=pdf.T,
    )

    # Divide by the normalizing constant, and the pdf is complete.
    numpy.divide(
        pdf, Dinv,
        where=i_eval, out=pdf,
    )

    return pdf


def conditional_q_pdf(
        q, m_1,
        beta_q, m_min, m_max, M_max,
        where=True, out=None,
    ):
    import numpy

    # Ensure everything is a numpy array.
    q = numpy.asarray(q)
    m_1 = numpy.asarray(m_1)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    q, m_1 = numpy.broadcast_arrays(q, m_1)
    beta_q, m_min, m_max = numpy.broadcast_arrays(beta_q, m_min, m_max)

    # Determine the shapes of the parameters and hyperparameters.
    S = q.shape
    T = beta_q.shape
    # Compute the output shape, as well as the shape of its transpose.
    TS = T+S
    TS_T = S[::-1]+T[::-1]

    # Broadcast `where` to the output shape.
    where_TS = numpy.broadcast_to(where.T, TS_T).T

    # Initialize the output array if not provided.
    out_type = q.dtype
    if out is None:
        pdf = numpy.zeros(TS, dtype=out_type)
    else:
        pdf = out
        pdf[where_TS] = 0.0

    # Create some temporary arrays to re-use later.
    tmp_TS = numpy.empty_like(pdf)
    tmp_i_T = numpy.empty(T, dtype=bool)

    # Compute q^beta, and store it in the output array.  We want this to have
    # Shape TS, but it will have shape TS_T if we don't transpose things, so we
    # perform transposes and inverse transposes.
    q_to_the_beta = numpy.power.outer(
        q.T, beta_q.T,
        where=where_TS.T, out=pdf.T,
    ).T

    ## Compute the minimum and maximum mass ratios for each m_1.
    ## Given in Equation 8 of LIGO-T1800428-v2.
    ## Want these both to have shape TS.
    # q_min = m_min / m_1
    q_min = numpy.divide.outer(m_min, m_1)
    # q_max = min(m_1, M_max-m_1) / m_1
    Mmax_minus_m1 = M_max - m_1
    min_m1_and_Mmax_minus_m1 = numpy.minimum(
        m_1, Mmax_minus_m1,
        out=Mmax_minus_m1,
    )
    q_max = numpy.divide(
        min_m1_and_Mmax_minus_m1, m_1,
        out=min_m1_and_Mmax_minus_m1,
    )
    q_max = 1.0
    q_max = numpy.broadcast_to(q_max, TS)

    # Compute the normalizing constant C(m_1).
    Cinv = numpy.empty_like(pdf)

    # Handle the special case of beta_q == -1 separately.
    i_neg_one_base = numpy.equal(beta_q, -1.0, out=tmp_i_T)
    i_neg_one = numpy.broadcast_to(i_neg_one_base, TS_T).T

    # Store ln(q_max) in C^-1 where it appears.
    ln_qmax = numpy.log(q_max, where=i_neg_one, out=Cinv)

    # Store ln(q_min) in tmp_TS, then subtract it from C^-1 where it appears.
    ln_qmin = numpy.log(q_min, where=i_neg_one, out=tmp_TS)
    numpy.subtract(ln_qmax, ln_qmin, where=i_neg_one, out=Cinv)

    # Handle the remaining non-special cases of beta != -1.
    i_nonspecial_base = numpy.invert(i_neg_one_base, out=tmp_i_T)
    i_nonspecial = numpy.broadcast_to(i_nonspecial_base, TS_T).T

    # Compute (1+beta_q).T as we will use this multiple times
    one_plus_beta_T = numpy.add(1.0, beta_q, where=where).T

    # Store q_max^(1+beta) in C^-1 where it appears.
    q_max_to_the_one_plus_beta = numpy.power(
        q_max.T, one_plus_beta_T,
        where=i_nonspecial.T, out=Cinv.T,
    ).T

    # Store q_min^(1+beta) in tmp_TS, then subtract it from C^-1 where it
    # appears.
    q_min_to_the_one_plus_beta = numpy.power(
        q_min.T, one_plus_beta_T,
        where=i_nonspecial.T, out=tmp_TS.T,
    ).T
    q_to_powers_difference = numpy.subtract(
        q_max_to_the_one_plus_beta, q_min_to_the_one_plus_beta,
        where=i_nonspecial, out=Cinv,
    )

    # Put in the 1/(1+beta) term in C^-1 and now it's complete.
    numpy.divide(
        Cinv.T, one_plus_beta_T,
        where=i_nonspecial.T, out=Cinv.T,
    )

    # Divide the PDF by C^-1 to get the final PDF on the support.
    numpy.divide(pdf, Cinv, where=where_TS, out=pdf)

    # Determine where there is no support and zero out the PDF
    i_nosupport_tmp = numpy.zeros(TS, dtype=bool)
    i_too_low = numpy.less(q, q_min, where=where_TS, out=i_nosupport_tmp)
    i_too_high = numpy.greater(q, q_max, where=where_TS)
    i_nosupport = numpy.logical_or(
        i_too_low, i_too_high,
        where=where_TS, out=i_nosupport_tmp,
    )
    pdf[i_nosupport] = 0.0

    return pdf


def conditional_m2_pdf(
        m_2, m_1,
        beta_q, m_min, m_max, M_max,
        where=True, out=None,
    ):
    import numpy

    # Ensure everything is a numpy array.
    m_2 = numpy.asarray(m_2)
    m_1 = numpy.asarray(m_1)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    m_2, m_1 = numpy.broadcast_arrays(m_2, m_1)
    beta_q, m_min, m_max = numpy.broadcast_arrays(beta_q, m_min, m_max)


    # Determine the shapes of the parameters and hyperparameters.
    S = m_2.shape
    T = beta_q.shape
    # Compute the shape of the transpose of the output array.
    TS_T = S[::-1]+T[::-1]

    # Broadcast `where` to the output shape.
    where_TS = numpy.broadcast_to(where.T, TS_T).T

    # Transform to the mass ratio.
    q = m_2 / m_1

    # Compute p(q | m_1)
    pdf_q_m1 = conditional_q_pdf(
        q, m_1,
        beta_q, m_min, m_max, M_max,
        where=where, out=out,
    )

    # Divide result by m_1 to get p(m_2 | m_1)
    pdf = numpy.divide(pdf_q_m1, m_1, where=where_TS, out=out)

    return pdf


def mass_joint_pdf(
        m_1, m_2,
        alpha_m, beta_q, m_min, m_max, M_max,
        const=None,
        out=None, where=True,
        rand_state=None,
    ):
    import numpy

    # Ensure everything is a numpy array.
    m_1 = numpy.asarray(m_1)
    m_2 = numpy.asarray(m_2)
    alpha_m = numpy.asarray(alpha_m)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    m_1, m_2 = numpy.broadcast_arrays(m_1, m_2)
    (
        alpha_m, beta_q, m_min, m_max,
    ) = numpy.broadcast_arrays(
        alpha_m, beta_q, m_min, m_max,
    )


    # Determine the shapes of the parameters and hyperparameters.
    S = m_1.shape
    T = alpha_m.shape
    # Compute the output shape, as well as the shape of its transpose.
    TS = T+S
    TS_T = S[::-1]+T[::-1]


    # Broadcast `where` to the output shape.
    where_TS = numpy.broadcast_to(where.T, TS_T).T
    # Determine where we need to evaluate the PDF.
    i_eval = where_TS & (m_1 + m_2 <= M_max)

    # Initialize the output array if not provided.
    out_type = m_1.dtype
    if out is None:
        pdf = numpy.zeros(TS, dtype=out_type)
    else:
        pdf = out
        pdf[where_TS] = 0.0

    if const is None:
        const = mass_pdf_const(
            alpha_m, beta_q, m_min, m_max, M_max,
            where=where,
            rand_state=rand_state,
        )

    tmp_TS = numpy.empty_like(pdf)

    # Compute p(m_1)
    pm1 = marginal_m1_pdf_noMmax(
        m_1,
        alpha_m, m_min, m_max,
        where=where, out=tmp_TS,
    )

    # Compute const * p(m_1)
    c_pm1 = numpy.multiply(
        numpy.broadcast_to(const.T, TS_T).T, pm1,
        where=i_eval, out=pdf,
    )

    # Compute p(m_2 | m_1)
    pm2 = conditional_m2_pdf(
        m_2, m_1,
        beta_q, m_min, m_max, M_max,
        where=where, out=tmp_TS,
    )

    # Compute const * p(m_1) * p(m_2 | m_1).
    numpy.multiply(pdf, pm2, where=i_eval, out=pdf)

    return pdf


def mass_pdf_const(
        alpha_m, beta_q, m_min, m_max, M_max,
        where=True,
        n_samples=10000,
        rand_state=None,
    ):
    import numpy

    if rand_state is None:
        rand_state = numpy.random.RandomState()


    # Ensure everything is a numpy array.
    alpha_m = numpy.asarray(alpha_m)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    (
        alpha_m, beta_q, m_min, m_max,
    ) = numpy.broadcast_arrays(
        alpha_m, beta_q, m_min, m_max,
    )

    n_samples_float = float(n_samples)

    const = numpy.empty_like(alpha_m)

    for idx, _ in numpy.ndenumerate(const):
        m1_m2 = mass_joint_rvs_noMmax(
            n_samples,
            alpha_m[idx], beta_q[idx], m_min[idx], m_max[idx],
            rand_state=rand_state,
        )

        i_bad = numpy.sum(m1_m2, axis=1) > M_max
        n_bad = numpy.count_nonzero(i_bad)

        if n_bad == 0:
            const[idx] = 1.0
        else:
            const[idx] = n_samples_float / n_bad

    return const


def mass_joint_rvs_noMmax(
        N,
        alpha_m, beta_q, m_min, m_max,
        rand_state=None,
    ):
    import numpy

    if rand_state is None:
        rand_state = numpy.random.RandomState()

    m_1 = powerlaw_rvs(N, alpha_m, m_min, m_max, rand_state=rand_state)

    q_min = m_min / m_1
    q_max = 1.0
#    q_max = numpy.minimum(m_1, M_max-m_1) / m_1
    q = powerlaw_rvs(N, -beta_q, q_min, q_max, rand_state=rand_state)
    m_2 = m_1 * q

    return numpy.column_stack((m_1, m_2))


def mass_joint_rvs(
        N,
        alpha_m, beta_q, m_min, m_max, M_max,
        rand_state=None,
    ):
    import numpy
    from ..prob import sample_with_cond

    if rand_state is None:
        rand_state = numpy.random.RandomState()

    def rvs(N):
        return mass_joint_rvs_noMmax(
            N,
            alpha_m, beta_q, m_min, m_max,
            rand_state=rand_state,
        )

    def cond(m1_m2):
        """
        Given an array, where each row contains a pair ``(m_1, m_2)``, returns
        an array whose value is ``True`` when ``m_1 + m_2 <= M_max`` and
        ``False`` otherwise.
        """
        return numpy.sum(m1_m2, axis=1) <= M_max

    return sample_with_cond(rvs, shape=N, cond=cond)






def joint_rvs(N, pop_params, M_max, rand_state=None):
    r"""
    Draws :math:`N` samples from the joint mass distribution :math:`p(m_1, m_2)`
    defined in :mod:`pop_models.powerlaw` as

    .. math::
       p(m_1, m_2) =
       C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
       \frac{m_1^{-\alpha}}{m_1 - m_{\mathrm{min}}}

    First draws samples from power law mass distribution :math:`p(m_1)`, then
    draws samples from the uniform distribution :math:`p(m_2 | m_1)`, and then
    rejects samples which do not satisfy the
    :math:`m_1 + m_2 \leq M_{\mathrm{max}}` constraint.
    """
    import numpy
    import scipy.stats

    from ..powerlaw.prob import joint_rvs as powerlaw_joint_rvs
    from ..utils import check_random_state

    rand_state = check_random_state(rand_state)

    (
        alpha_m, beta_q, m_min, m_max,
        E_chi1, Var_chi1, mu_cos1, sigma_cos1,
        E_chi2, Var_chi2, mu_cos2, sigma_cos2,
    ) = pop_params[1:]

    alpha_chi1, beta_chi1 = spin_mu_sigma_sq_2_alpha_beta(E_chi1, Var_chi1)
    alpha_chi2, beta_chi2 = spin_mu_sigma_sq_2_alpha_beta(E_chi2, Var_chi2)

    m1_m2 = mass_joint_rvs(
        N,
        alpha_m, beta_q, m_min, m_max, M_max,
        rand_state=rand_state,
    )

    Beta_1 = scipy.stats.beta(alpha_chi1, beta_chi1)
    Beta_2 = scipy.stats.beta(alpha_chi2, beta_chi2)

    chi_1 = Beta_1.rvs(N, random_state=rand_state)
    chi_2 = Beta_2.rvs(N, random_state=rand_state)

    Gauss_1 = tilt_model(mu_cos1, sigma_cos1)
    Gauss_2 = tilt_model(mu_cos2, sigma_cos2)

    cos_theta_1 = Gauss_1.rvs(N, random_state=rand_state)
    cos_theta_2 = Gauss_2.rvs(N, random_state=rand_state)

    return numpy.column_stack((
        m1_m2,
        chi_1, chi_2,
        cos_theta_1, cos_theta_2,
    ))


def joint_pdf(
        indiv_params, pop_params, M_max,
        const=None,
        out=None, where=True,
    ):
    r"""
    Computes the probability density for the joint mass distribution
    :math:`p(m_1, m_2, \chi_1, \chi_2)` defined in
    :mod:`pop_models.powerlaw_spin_mag` as

    .. math::
       p(m_1, m_2, \chi_1, \chi_2) =
       C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
       \frac{m_1^{-\alpha}}{m_1 - m_{\mathrm{min}}} \,
       \mathrm{Beta}(\chi_1; \alpha_{\chi_1}, \beta_{\chi_1}) \,
       \mathrm{Beta}(\chi_2; \alpha_{\chi_2}, \beta_{\chi_2})

    Computes the normalization constant using :func:`pdf_const` if not provided
    by the ``const`` keyword argument.
    """
    import numpy

    from ..powerlaw.prob import joint_pdf as powerlaw_joint_pdf
    from .. import stats as pop_models_stats

    m_1, m_2, chi_1, chi_2, cos_theta_1, cos_theta_2 = indiv_params.T

    (
        alpha_m, beta_q, m_min, m_max,
        E_chi1, Var_chi1, mu_cos1, sigma_cos1,
        E_chi2, Var_chi2, mu_cos2, sigma_cos2,
    ) = pop_params.T[1:]

    alpha_chi1, beta_chi1 = spin_mu_sigma_sq_2_alpha_beta(E_chi1, Var_chi1)
    alpha_chi2, beta_chi2 = spin_mu_sigma_sq_2_alpha_beta(E_chi2, Var_chi2)

    (
        m_1, m_2,
        chi_1, chi_2,
        cos_theta_1, cos_theta_2,
    ) = numpy.broadcast_arrays(
        m_1, m_2,
        chi_1, chi_2,
        cos_theta_1, cos_theta_2,
    )

    (
        alpha_m, beta_q, m_min, m_max,
        alpha_chi1, beta_chi1, mu_cos1, sigma_cos1,
        alpha_chi2, beta_chi2, mu_cos2, sigma_cos2,
    ) = numpy.broadcast_arrays(
        alpha_m, beta_q, m_min, m_max,
        alpha_chi1, beta_chi1, mu_cos1, sigma_cos1,
        alpha_chi2, beta_chi2, mu_cos2, sigma_cos2,
    )

    m_1 = numpy.asarray(m_1)
    m_2 = numpy.asarray(m_2)
    chi_1 = numpy.asarray(chi_1)
    chi_2 = numpy.asarray(chi_2)
    cos_theta_1 = numpy.asarray(cos_theta_1)
    cos_theta_2 = numpy.asarray(cos_theta_2)

    alpha_m = numpy.asarray(alpha_m)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    alpha_chi1 = numpy.asarray(alpha_chi1)
    beta_chi1 = numpy.asarray(beta_chi1)
    mu_cos1 = numpy.asarray(mu_cos1)
    sigma_cos1 = numpy.asarray(sigma_cos1)
    alpha_chi2 = numpy.asarray(alpha_chi2)
    beta_chi2 = numpy.asarray(beta_chi2)
    mu_cos2 = numpy.asarray(mu_cos2)
    sigma_cos2 = numpy.asarray(sigma_cos2)

    where = numpy.asarray(where)

    S = m_1.shape
    T = alpha_m.shape
    TS = T+S

    out_type = m_1.dtype

    # Create a version of where which has shape ``T + S``.
    where_TS = numpy.broadcast_to(where.T, S[::-1]+T[::-1]).T

    # Initialize the output array, ``pdf``, as well as another array of the same
    # shape, ``tmp``, for some intermediate results.  We're going to store each
    # contribution to ``pdf`` in ``tmp``, and then multiply it on.
    if out is None:
        pdf = numpy.zeros(TS, dtype=out_type)
    else:
        pdf = out
        pdf[where_TS] = 0.0

    tmp = numpy.empty_like(pdf)

    # Mass contributions
    mass_joint_pdf(
        m_1, m_2,
        alpha_m, beta_q, m_min, m_max, M_max,
        const=const,
        out=pdf, where=where,
    )
    # Spin magnitude contributions
    pop_models_stats.beta_pdf(
        chi_1, alpha_chi1, beta_chi1,
        out=tmp, where=where_TS,
    )
    numpy.multiply(pdf, tmp, out=pdf, where=where_TS)
    pop_models_stats.beta_pdf(
        chi_2, alpha_chi2, beta_chi2,
        out=tmp, where=where_TS,
    )
    numpy.multiply(pdf, tmp, out=pdf, where=where_TS)
    # Spin tilt contributions
    pop_models_stats.truncnorm_pdf(
        cos_theta_1,
        mu_cos1, sigma_cos1,
        -1.0, +1.0,
        out=tmp, where=where_TS,
    )
    numpy.multiply(pdf, tmp, out=pdf, where=where_TS)
    pop_models_stats.truncnorm_pdf(
        cos_theta_2,
        mu_cos2, sigma_cos2,
        -1.0, +1.0,
        out=tmp, where=where_TS,
    )
    numpy.multiply(pdf, tmp, out=pdf, where=where_TS)

    return pdf


def marginal_m1_pdf(m1, alpha, m_min, m_max, M_max, const=None):
    r"""
    Computes the probability density for the marginal mass distribution
    :math:`p(m_1)` defined in :mod:`pop_models.powerlaw` as

    .. math::
       p(m_1, m_2) =
       C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
       m_1^{-\alpha} \,
       \frac{\min(m_1, M_{\mathrm{max}}-m_1) - m_{\mathrm{min}}}
            {m_1 - m_{\mathrm{min}}}

    Computes the normalization constant using :func:`pdf_const` if not provided
    by the ``const`` keyword argument.
    """
    from ..powerlaw.prob import marginal_pdf

    return marginal_pdf(m1, alpha, m_min, m_max, M_max, const=const)


def marginal_spin_mag_pdf(chi, alpha_chi, beta_chi):
    import numpy
    import scipy.stats

    from . import utils

    chi = numpy.asarray(chi)
    alpha_chis, beta_chis = utils.upcast_scalars((alpha_chi, beta_chi))

    pdf = numpy.zeros((len(alpha_chis), len(chi)), dtype=chi.dtype)

    for i, (alpha_chi, beta_chi) in enumerate(zip(alpha_chis, beta_chis)):
        pdf[i] = scipy.stats.beta(alpha_chi, beta_chi).pdf(chi)

    return pdf


def marginal_spin_tilt_pdf(cos_theta, mu_cos, sigma_cos):
    import numpy

    from . import utils

    cos_theta = numpy.asarray(cos_theta)
    mu_coses, sigma_coses = utils.upcast_scalars((mu_cos, sigma_cos))

    pdf = numpy.zeros((len(sigma_coses), len(cos_theta)), dtype=cos_theta.dtype)

    for i, (mu_cos, sigma_cos) in enumerate(zip(mu_coses, sigma_coses)):
        Gauss = tilt_model(mu_cos, sigma_cos)
        pdf[i] = Gauss.pdf(cos_theta)

    return pdf
