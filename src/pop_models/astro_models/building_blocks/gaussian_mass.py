import types
import typing

import numpy
import scipy.special
import scipy.stats

from pop_models.coordinate import CoordinateSystem
from pop_models.population import Population
from pop_models.types import Numeric, WhereType, Observables, Parameters

from pop_models.astro_models import coordinates

from pop_models.utils import debug_verbose

coordinate_system = CoordinateSystem(
    coordinates.m1_source_coord,
    coordinates.m2_source_coord,
)
coordinate_system_m1 = CoordinateSystem(coordinates.m1_source_coord)

def truncnorm(mu, sigma, lower, upper):
    a, b = (lower-mu)/sigma, (upper-mu)/sigma
    return scipy.stats.truncnorm(a, b, loc=mu, scale=sigma)


def truncnorm_rvs(
        n_samples: int,
        mu: Numeric, sigma: Numeric, a: Numeric, b: Numeric,
        random_state: typing.Optional[numpy.random.RandomState]=None,
    ) -> Numeric:
    """
    Draws random variables from a truncated normal distribution via inverse CDF
    sampling.

    Follows expressions given in https://en.wikipedia.org/wiki/Truncated_normal_distribution#Generating_values_from_the_truncated_normal_distribution
    """
    if random_state is None:
        random_state = numpy.random.RandomState()

    # Ensure all inputs are arrays.
    mu = numpy.asarray(mu)
    sigma = numpy.asarray(sigma)
    a = numpy.asarray(a)
    b = numpy.asarray(b)

    # Compute array shapes.
    # Shape of parameters.
    params_shape = numpy.shape(mu)
    # Shape of final output.
    out_shape = params_shape + (n_samples,)
    # Shape of final output, transposed.
    out_shape_T = tuple(reversed(out_shape))

    # Draw random values for CDF to take, in transposed-space.
    U_T = random_state.uniform(size=out_shape_T)

    # Evaluate rescaled boundary terms, in transposed-space.
    alpha_T = (a.T - mu.T) / sigma.T
    beta_T = (b.T - mu.T) / sigma.T

    # Evaluate Gaussian CDF terms, in transposed-space.
    Phi_alpha_T = scipy.stats.norm.cdf(alpha_T)
    Phi_beta_T = scipy.stats.norm.cdf(beta_T)

    # Evaluate the difference between the Gaussian CDF terms, in
    # transposed-space.
    Z_T = Phi_beta_T - Phi_alpha_T

    # Evaluate the inverse Gaussian CDF term in the transpose-space.
    Phi_inv_T = scipy.stats.norm.ppf(U_T*Z_T + Phi_alpha_T)

    # Compute the value of the output samples, in the transpose-space.
    x_T = sigma.T * Phi_inv_T + mu.T

    # Return final, un-transposed result.
    return x_T.T


class ComponentMassIIDGaussianPopulation(Population):
    def __init__(
            self,
            xpy: types.ModuleType=numpy,
            rate_name: str="rate",
            mean_name: str="m_mean", stdev_name: str="m_stdev",
        ):
        param_names = (rate_name, mean_name, stdev_name)
        correlated_coords = frozenset({frozenset(coordinate_system)})

        super().__init__(
            coordinate_system, param_names,
            transformations=coordinates.transformations,
            correlated_coords=correlated_coords,
        )

        self.xpy = xpy

        self.rate_name = rate_name
        self.mean_name = mean_name
        self.stdev_name = stdev_name

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        from  ... import stats as pop_models_stats

        m1_source, m2_source = observables

        mean = parameters[self.mean_name]
        stdev = parameters[self.stdev_name]

        param_shape = mean.shape
        where_arr = self.xpy.broadcast_to(where, param_shape)
        cond = self.xpy.logical_and.outer(where_arr, m1_source >= m2_source)

        pm1 = pop_models_stats.truncnorm_pdf(
            m1_source,
            mean, stdev,
            0.0, numpy.inf,
            where=where_arr,
        )
        pm2 = pop_models_stats.truncnorm_pdf(
            m2_source,
            mean, stdev,
            0.0, numpy.inf,
            where=where_arr,
        )

        pm1m2 = self.xpy.multiply(pm1, pm2, out=pm1)
        pm1m2 *= 2.0

        return self.xpy.where(cond, pm1m2, 0.0)


    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        mean = parameters[self.mean_name]
        stdev = parameters[self.stdev_name]

        param_shape = mean.shape
        out_shape = param_shape + (n_samples,)

        where = self.xpy.broadcast_to(where, param_shape)

        m1_samples = self.xpy.ones(out_shape, dtype=numpy.float64)
        m2_samples = self.xpy.ones(out_shape, dtype=numpy.float64)

        ## TODO: vectorize loop by rewriting truncnorm rvs function
        for idx in numpy.ndindex(*param_shape):
            # Skip samples that are ruled out by `where`.
            if not where[idx]:
                continue

            dist = truncnorm(mean[idx], stdev[idx], 0.0, numpy.inf)

            ma_samples = dist.rvs(n_samples, random_state=random_state)
            mb_samples = dist.rvs(n_samples, random_state=random_state)

            i_geq = ma_samples >= mb_samples

            m1_samples[idx] = self.xpy.where(i_geq, ma_samples, mb_samples)
            m2_samples[idx] = self.xpy.where(i_geq, mb_samples, ma_samples)

        return m1_samples, m2_samples


    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]


class ComponentMassGaussianPopulation(Population):
    def __init__(
            self,
            xpy: types.ModuleType=numpy,
            same_mass_cutoffs: bool=True,
            rate_name: str="rate",
            mean1_name: str="m1_mean", stdev1_name: str="m1_stdev",
            mean2_name: str="m2_mean", stdev2_name: str="m2_stdev",
            mmin1_name: str="m1_min", mmax1_name: str="m1_max",
            mmin2_name: str="m2_min", mmax2_name: str="m2_max",
            mmin_name: str="m_min", mmax_name: str="m_max",
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ):
        param_names_base = (
            rate_name,
            mean1_name, stdev1_name, mean2_name, stdev2_name,
        )
        if same_mass_cutoffs:
            param_names_cutoffs = (
                mmin_name, mmax_name,
            )
        else:
            param_names_cutoffs = (
                mmin1_name, mmax1_name,
                mmin2_name, mmax2_name,
            )

        param_names = param_names_base + param_names_cutoffs

        correlated_coords = frozenset({frozenset(coordinate_system)})

        super().__init__(
            coordinate_system, param_names,
            transformations=coordinates.transformations,
            correlated_coords=correlated_coords,
        )

        self.xpy = xpy

        self.same_mass_cutoffs = same_mass_cutoffs

        self.rate_name = rate_name
        self.mean1_name = mean1_name
        self.stdev1_name = stdev1_name
        self.mean2_name = mean2_name
        self.stdev2_name = stdev2_name
        self.mmin1_name = mmin1_name
        self.mmin2_name = mmin2_name
        self.mmax1_name = mmax1_name
        self.mmax2_name = mmax2_name
        self.mmin_name = mmin_name
        self.mmax_name = mmax_name

        self.random_state = random_state

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        import pop_models.stats as pop_models_stats

        m1_source, m2_source = observables

        mean1 = parameters[self.mean1_name]
        stdev1 = parameters[self.stdev1_name]
        mean2 = parameters[self.mean2_name]
        stdev2 = parameters[self.stdev2_name]

        if self.same_mass_cutoffs:
            m1_min = m2_min = parameters[self.mmin_name]
            m1_max = m2_max = parameters[self.mmax_name]
        else:
            m1_min = parameters[self.mmin1_name]
            m1_max = parameters[self.mmax1_name]
            m2_min = parameters[self.mmin2_name]
            m2_max = parameters[self.mmax2_name]

        param_shape = mean1.shape
        where_arr = self.xpy.broadcast_to(where, param_shape)

        cond = self.xpy.logical_and.outer(where_arr, m1_source >= m2_source)

        pm1 = pop_models_stats.truncnorm_pdf(
            m1_source,
            mean1, stdev1,
            m1_min, m1_max,
            where=where_arr,
        )
        pm2 = pop_models_stats.truncnorm_pdf(
            m2_source,
            mean2, stdev2,
            m2_min, m2_max,
            where=where_arr,
        )

        # Compute (incorectly normalized) PDF.
        pm1m2 = self.xpy.multiply(pm1, pm2, out=pm1)

        # Compute normalization factor for PDF by drawing 4096 samples and
        # counting the fraction that are kept or rejected.
        n_samples = 4096
        m1_samples = truncnorm_rvs(
            n_samples,
            mean1, stdev1, m1_min, m1_max,
            random_state=self.random_state,
        )
        m2_samples = truncnorm_rvs(
            n_samples,
            mean2, stdev2, m2_min, m2_max,
            random_state=self.random_state,
        )
        i_kept = m1_samples >= m2_samples
        n_kept = numpy.count_nonzero(i_kept, axis=-1)
        # Apply normalization correction, `(n_samples / n_kept)`
        pm1m2 *= n_samples
        pm1m2 /= n_kept[...,numpy.newaxis]

        return self.xpy.where(cond, pm1m2, 0.0)


    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        mean1 = parameters[self.mean1_name]
        stdev1 = parameters[self.stdev1_name]
        mean2 = parameters[self.mean2_name]
        stdev2 = parameters[self.stdev2_name]

        if self.same_mass_cutoffs:
            m1_min = m2_min = parameters[self.mmin_name]
            m1_max = m2_max = parameters[self.mmax_name]
        else:
            m1_min = parameters[self.mmin1_name]
            m1_max = parameters[self.mmax1_name]
            m2_min = parameters[self.mmin2_name]
            m2_max = parameters[self.mmax2_name]

        param_shape = mean1.shape
        out_shape = param_shape + (n_samples,)

        where = self.xpy.broadcast_to(where, param_shape)

        m1_samples = self.xpy.ones(out_shape, dtype=numpy.float64)
        m2_samples = self.xpy.ones(out_shape, dtype=numpy.float64)

        for idx in numpy.ndindex(*param_shape):
            # Skip samples that are ruled out by `where`.
            if not where[idx]:
                continue

            i_bad = True
            n_bad = n_samples
            while numpy.any(i_bad):
                m1_samples[idx][i_bad] = truncnorm(
                    mean1[idx], stdev1[idx], m1_min[idx], m1_max[idx],
                ).rvs(n_bad, random_state=random_state)
                m2_samples[idx][i_bad] = truncnorm(
                    mean2[idx], stdev2[idx], m2_min[idx], m2_max[idx],
                ).rvs(n_bad, random_state=random_state)

                i_bad = m2_samples[idx] > m1_samples[idx]
                n_bad = numpy.count_nonzero(i_bad)

        return m1_samples, m2_samples


    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]


    def params_valid(self, parameters: Parameters) -> WhereType:
        mean1 = parameters[self.mean1_name]
        mean2 = parameters[self.mean2_name]

        if self.same_mass_cutoffs:
            mmin1 = mmin2 = parameters[self.mmin_name]
            mmax1 = mmax2 = parameters[self.mmax_name]
        else:
            mmin1 = parameters[self.mmin1_name]
            mmax1 = parameters[self.mmax1_name]
            mmin2 = parameters[self.mmin2_name]
            mmax2 = parameters[self.mmax2_name]

        is_valid = mean1 >= mean2
        debug_verbose(
            "Gaussian means enforce m1,m2 sorting?", is_valid,
            mode="params_valid_gaussian_mass",
        )

        # Initialize array to re-use for all other comparisons, to save on
        # memory.
        is_valid_tmp = self.xpy.empty_like(is_valid)

        large_enough = self.xpy.less_equal(mmin1, mean1, out=is_valid_tmp)
        debug_verbose(
            "Gaussian mean 1 large enough?", large_enough,
            mode="params_valid_gaussian_mass",
        )
        is_valid &= large_enough

        large_enough = self.xpy.less_equal(mmin2, mean2, out=is_valid_tmp)
        debug_verbose(
            "Gaussian mean 2 large enough?", large_enough,
            mode="params_valid_gaussian_mass",
        )
        is_valid &= large_enough

        small_enough = self.xpy.less_equal(mean1, mmax1, out=is_valid_tmp)
        debug_verbose(
            "Gaussian mean 1 small enough?", small_enough,
            mode="params_valid_gaussian_mass",
        )
        is_valid &= small_enough

        small_enough = self.xpy.less_equal(mean2, mmax2, out=is_valid_tmp)
        debug_verbose(
            "Gaussian mean 2 small enough?", small_enough,
            mode="params_valid_gaussian_mass", flush=True,
        )
        is_valid &= small_enough

        return is_valid

    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> Population:
        if self.coord_system == coord_system:
            return self

        if coordinate_system_m1 == coord_system:
            return M1ComponentMassGaussianPopulation(
                xpy=self.xpy,
                same_mass_cutoffs=self.same_mass_cutoffs,
                rate_name=self.rate_name,
                mean1_name=self.mean1_name, stdev1_name=self.stdev1_name,
                mean2_name=self.mean2_name, stdev2_name=self.stdev2_name,
                mmin1_name=self.mmin1_name, mmax1_name=self.mmax1_name,
                mmin2_name=self.mmin2_name, mmax2_name=self.mmax2_name,
                mmin_name=self.mmin_name, mmax_name=self.mmax_name,
                random_state=self.random_state,
            )

        return super().to_coords(
            coord_system,
            use_transformations=use_transformations,
        )


class M1ComponentMassGaussianPopulation(ComponentMassGaussianPopulation):
    def __init__(
            self,
            xpy: types.ModuleType=numpy,
            same_mass_cutoffs: bool=True,
            rate_name: str="rate",
            mean1_name: str="m1_mean", stdev1_name: str="m1_stdev",
            mean2_name: str="m2_mean", stdev2_name: str="m2_stdev",
            mmin1_name: str="m1_min", mmax1_name: str="m1_max",
            mmin2_name: str="m2_min", mmax2_name: str="m2_max",
            mmin_name: str="m_min", mmax_name: str="m_max",
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ):
        param_names_base = (
            rate_name,
            mean1_name, stdev1_name, mean2_name, stdev2_name,
        )
        if same_mass_cutoffs:
            param_names_cutoffs = (
                mmin_name, mmax_name,
            )
        else:
            param_names_cutoffs = (
                mmin1_name, mmax1_name,
                mmin2_name, mmax2_name,
            )

        param_names = param_names_base + param_names_cutoffs

        correlated_coords = frozenset({frozenset(coordinate_system)})

        super(ComponentMassGaussianPopulation, self).__init__(
            coordinate_system_m1, param_names,
            transformations=coordinates.transformations,
            correlated_coords=correlated_coords,
        )

        self.xpy = xpy

        self.same_mass_cutoffs = same_mass_cutoffs

        self.rate_name = rate_name
        self.mean1_name = mean1_name
        self.stdev1_name = stdev1_name
        self.mean2_name = mean2_name
        self.stdev2_name = stdev2_name
        self.mmin1_name = mmin1_name
        self.mmin2_name = mmin2_name
        self.mmax1_name = mmax1_name
        self.mmax2_name = mmax2_name
        self.mmin_name = mmin_name
        self.mmax_name = mmax_name

        self.random_state = random_state

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        import pop_models.stats as pop_models_stats

        m1_source, = observables

        mean1 = parameters[self.mean1_name]
        stdev1 = parameters[self.stdev1_name]
        mean2 = parameters[self.mean2_name]
        stdev2 = parameters[self.stdev2_name]

        if self.same_mass_cutoffs:
            m1_min = m2_min = parameters[self.mmin_name]
            m1_max = m2_max = parameters[self.mmax_name]
        else:
            m1_min = parameters[self.mmin1_name]
            m1_max = parameters[self.mmax1_name]
            m2_min = parameters[self.mmin2_name]
            m2_max = parameters[self.mmax2_name]

        param_shape = mean1.shape
        obs_shape = m1_source.shape
        where_arr = self.xpy.broadcast_to(where, param_shape)

        # Slice that converts parameter shape to output shape.
        slc_param2out = (...,) + (numpy.newaxis,)*len(obs_shape)

        # Compute (incorectly normalized) PDF.
        pm1 = pop_models_stats.truncnorm_pdf(
            m1_source,
            mean1, stdev1,
            m1_min, m1_max,
            where=where_arr,
        )

        # Compute normalization factor for PDF by drawing 4096 samples and
        # counting the fraction that are kept or rejected.
        n_samples = 4096
        m1_samples = truncnorm_rvs(
            n_samples,
            mean1, stdev1, m1_min, m1_max,
            random_state=self.random_state,
        )
        m2_samples = truncnorm_rvs(
            n_samples,
            mean2, stdev2, m2_min, m2_max,
            random_state=self.random_state,
        )
        i_kept = m1_samples >= m2_samples
        n_kept = numpy.count_nonzero(i_kept, axis=-1)
        # Apply normalization correction, `(n_samples / n_kept)`
        pm1 *= n_samples
        pm1 /= n_kept[...,numpy.newaxis]

        # Compute normalization factor from marginalizing over m2.
        inv_sqrt2 = 2.0**-0.5
        def Phi(x):
            return 0.5 * (1.0 + scipy.special.erf(x * inv_sqrt2))

        Phi_min = Phi((m2_min - mean2) / stdev2)
        Phi_max = Phi((m2_max - mean2) / stdev2)
        Phi_m1 = Phi(
            (numpy.minimum.outer(m2_max, m1_source) - mean2[slc_param2out]) /
            stdev2[slc_param2out]
        )

        pm1 *= (
            (Phi_m1 - Phi_min[slc_param2out]) /
            (Phi_max - Phi_min)[slc_param2out]
        )

        return pm1

        ## OLD CODE
        m2_samples = truncnorm_rvs(
            n_samples,
            mean2, stdev2, m2_min, m2_max,
            random_state=self.random_state,
        )
        # Create views of `m1_source` and `m2_samples` compatible with shape
        # `params_shape + obs_shape + (n_samples,)`.
        # For `m1_source`, we just need to add the `(n_samples,)` part on the
        # end with an empty axis.
        # For `m2_samples`, we need to insert the `obs_shape` portion in the
        # middle, which requires manually placing the correct number of empty
        # slices on either end (equivalent of `:`) and the correct number of
        # empty axes in the middle.
        m1_source_view = m1_source[...,None]
        m2_samples_view = m2_samples[
            tuple([slice(None)]*len(param_shape)) +
            tuple([None]*len(obs_shape)) +
            (slice(None),)
        ]
        i_kept = m1_source_view >= m2_samples_view
        n_kept = numpy.count_nonzero(i_kept, axis=-1)
        # Apply normalization correction, `(n_samples / n_kept)`
        pm1 *= n_samples
        pm1 /= n_kept

        # Replace `nan` with `0.0`.
        pm1[n_kept == 0] = 0.0

        return pm1


    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        m1_samples, m2_samples = super().rvs(
            n_samples, parameters,
            where=where,
            random_state=random_state,
            **kwargs
        )

        return m1_samples,
