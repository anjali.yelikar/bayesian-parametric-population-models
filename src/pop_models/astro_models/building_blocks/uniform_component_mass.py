import types

import numpy

from pop_models.coordinate import CoordinateSystem
from pop_models.population import Population
from pop_models.types import Numeric, WhereType, Observables, Parameters

from pop_models.astro_models import coordinates

from pop_models.utils import debug_verbose

coordinate_system = CoordinateSystem(
    coordinates.m1_source_coord,
    coordinates.m2_source_coord,
)
coordinate_system_m1 = CoordinateSystem(coordinates.m1_source_coord)


def inv_norm_const(
        mmin1, mmax1, mmin2, mmax2,
        xpy: types.ModuleType=numpy,
    ):
    is_rectangular = mmax2 <= mmin1

    dm1 = mmax1 - mmin1
    dm2 = mmax2 - mmin2

    norm_rectangular = dm1*dm2
    norm_triangle = 0.5 * dm2**2

    return xpy.where(is_rectangular, norm_rectangular, norm_triangle)

def norm_const(
        mmin1, mmax1, mmin2, mmax2,
        xpy: types.ModuleType=numpy,
    ):
    return xpy.reciprocal(inv_norm_const(mmin1, mmax1, mmin2, mmax2, xpy=xpy))


class UniformComponentMassPopulation(Population):
    def __init__(
            self,
            rate_name: str="rate",
            mmin1_name: str="m1_min", mmax1_name: str="m1_max",
            mmin2_name: str="m2_min", mmax2_name: str="m2_max",
            xpy: types.ModuleType=numpy,
        ):
        import warnings
        warnings.warn(
            "Currently this only supports populations with rectangular or "
            "triangular support.  The 'params_valid' method is extra "
            "restrictive to help enforce this."
        )

        param_names = (
            rate_name,
            mmin1_name, mmax1_name,
            mmin2_name, mmax2_name,
        )

        correlated_coords = frozenset({frozenset(coordinate_system)})

        super().__init__(
            coordinate_system, param_names,
            transformations=coordinates.transformations,
            correlated_coords=correlated_coords,
        )

        self.xpy = xpy

        self.rate_name = rate_name
        self.mmin1_name = mmin1_name
        self.mmin2_name = mmin2_name
        self.mmax1_name = mmax1_name
        self.mmax2_name = mmax2_name

        self.marginal_m1_pop = MarginalM1UniformComponentMassPopulation(
            rate_name=rate_name,
            mmin1_name=mmin1_name, mmax1_name=mmax1_name,
            mmin2_name=mmin2_name, mmax2_name=mmax2_name,
            xpy=xpy,
        )

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        # Unpack arguments
        m1, m2 = observables

        mmin1 = self.xpy.asarray(parameters[self.mmin1_name])
        mmax1 = self.xpy.asarray(parameters[self.mmax1_name])
        mmin2 = self.xpy.asarray(parameters[self.mmin2_name])
        mmax2 = self.xpy.asarray(parameters[self.mmax2_name])

        # Determine output shape
        obs_shape = m1.shape
        param_shape = mmin1.shape
        shape = param_shape + obs_shape

        # Create indices for broadcasting both observables and parameters into
        # compatible views.
        obs_broadcast_slice = (None,)*len(param_shape) + (...,)
        param_broadcast_slice = (...,) + (None,)*len(obs_shape)

        # Compute the normalization constant
        C = norm_const(mmin1, mmax1, mmin2, mmax2, xpy=self.xpy)

        # Reshape the normalization constant to the shape of the output array,
        # with extra dimensions prepended to allow for m1-dependance.
        p = self.xpy.broadcast_to(C, shape).copy()

        # Zero-out indices outside the range of support.
        p[..., m1 < m2] = 0.0

        m1_broadcast = m1[obs_broadcast_slice]
        p[mmin1[param_broadcast_slice] > m1_broadcast] = 0.0
        p[mmax1[param_broadcast_slice] < m1_broadcast] = 0.0

        m2_broadcast = m2[obs_broadcast_slice]
        p[mmin2[param_broadcast_slice] > m2_broadcast] = 0.0
        p[mmax2[param_broadcast_slice] < m2_broadcast] = 0.0

        return p

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        if random_state is None:
            random_state = self.xpy.random.RandomState()

        # Unpack arguments
        mmin1 = self.xpy.asarray(parameters[self.mmin1_name])
        mmax1 = self.xpy.asarray(parameters[self.mmax1_name])
        mmin2 = self.xpy.asarray(parameters[self.mmin2_name])
        mmax2 = self.xpy.asarray(parameters[self.mmax2_name])

        # Determine output shape
        obs_shape = (n_samples,)
        param_shape = mmin1.shape
        shape = obs_shape + param_shape

        # Create indices for broadcasting both observables and parameters into
        # compatible views.
        obs_broadcast_slice = (None,)*len(obs_shape) + (...,)
        param_broadcast_slice = (...,) + (None,)*len(obs_shape)

        # Sample from p(m1)
        m1_samples, = self.marginal_m1_pop.rvs(
            n_samples, parameters, where=where,
            random_state=random_state,
            **kwargs
        )

        # Sample from p(m2 | m1)
        mmax2_actual = self.xpy.minimum(
            m1_samples,
            mmax2[param_broadcast_slice],
        )
        m2_samples = random_state.uniform(
            mmin2[param_broadcast_slice], mmax2_actual,
        )

        return m1_samples, m2_samples

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]

    def params_valid(self, parameters: Parameters) -> WhereType:
        """
        Returns a boolean array specifying where the parameters are valid.
        """
        # Unpack arguments
        mmin1 = self.xpy.asarray(parameters[self.mmin1_name])
        mmax1 = self.xpy.asarray(parameters[self.mmax1_name])
        mmin2 = self.xpy.asarray(parameters[self.mmin2_name])
        mmax2 = self.xpy.asarray(parameters[self.mmax2_name])

        params_shape = self.xpy.shape(mmin1)

        # Initialize result array and a temporary array which is reused for the
        # comparisons.
        result = self.xpy.empty(params_shape, dtype=bool)
        tmp = self.xpy.empty(params_shape, dtype=bool)

        # Write the first comparison directly to the result array.
        self.xpy.greater(mmax1, mmin1, out=result)

        # Write the remaining comparisons to `tmp`, and then update `result`
        # with the bitwise-and between it and `tmp`.
        self.xpy.greater(mmax2, mmin2, out=tmp)
        result &= tmp

        self.xpy.greater_equal(mmin1, mmin2, out=tmp)
        result &= tmp

        self.xpy.greater_equal(mmax1, mmax2, out=tmp)
        result &= tmp

        self.xpy.greater(mmin1, 0.0, out=tmp)
        result &= tmp

        self.xpy.greater(mmin2, 0.0, out=tmp)
        result &= tmp

        # TEMPORARY: Also enforce that the parameters either correspond to a
        # rectangular or triangular region of support.
        result &= (mmax2 <= mmin1) | ((mmin1 == mmin2) & (mmax1 == mmax2))

        return result


class MarginalM1UniformComponentMassPopulation(Population):
    def __init__(
            self,
            rate_name: str="rate",
            mmin1_name: str="m1_min", mmax1_name: str="m1_max",
            mmin2_name: str="m2_min", mmax2_name: str="m2_max",
            xpy: types.ModuleType=numpy,
        ):
        param_names = (
            rate_name,
            mmin1_name, mmax1_name,
            mmin2_name, mmax2_name,
        )

        super().__init__(
            coordinate_system_m1, param_names,
            transformations=coordinates.transformations,
        )

        self.xpy = xpy

        self.rate_name = rate_name
        self.mmin1_name = mmin1_name
        self.mmin2_name = mmin2_name
        self.mmax1_name = mmax1_name
        self.mmax2_name = mmax2_name

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        # Unpack arguments
        m1, = observables

        mmin1 = self.xpy.asarray(parameters[self.mmin1_name])
        mmax1 = self.xpy.asarray(parameters[self.mmax1_name])
        mmin2 = self.xpy.asarray(parameters[self.mmin2_name])
        mmax2 = self.xpy.asarray(parameters[self.mmax2_name])

        # Determine output shape
        obs_shape = m1.shape
        param_shape = mmin1.shape
        shape = param_shape + obs_shape

        # Create indices for broadcasting both observables and parameters into
        # compatible views.
        obs_broadcast_slice = (None,)*len(param_shape) + (...,)
        param_broadcast_slice = (...,) + (None,)*len(obs_shape)

        # Compute the resulting PDF assuming support
        p_support = norm_const(mmin1, mmax1, mmin2, mmax2, xpy=self.xpy)

        # Make a copy of the PDF assuming support, with extra dimensions
        # prepended to allow non-supported (m1,m2) to set the result to zero.
        p = self.xpy.broadcast_to(p_support, shape).copy()

        # Zero-out indices outside the rectangle of support.
        m1_broadcast = m1[obs_broadcast_slice]
        i_m1_unsupported = (
            (mmin1[param_broadcast_slice] > m1_broadcast) |
            (mmax1[param_broadcast_slice] < m1_broadcast)
        )
        p[i_m1_unsupported] = 0.0
        del i_m1_unsupported

        # Multiply on m1-dependance
        p *= self.xpy.minimum(m1_broadcast, mmax2) - mmin2

        # Return result
        return p


    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        if random_state is None:
            random_state = self.xpy.random.RandomState()

        # Unpack arguments
        mmin1 = self.xpy.asarray(parameters[self.mmin1_name])
        mmax1 = self.xpy.asarray(parameters[self.mmax1_name])
        mmin2 = self.xpy.asarray(parameters[self.mmin2_name])
        mmax2 = self.xpy.asarray(parameters[self.mmax2_name])

        # Determine output shape
        param_shape = mmin1.shape
        shape = param_shape + (n_samples,)

        # Initialize output
        m1_samples = self.xpy.empty(shape)

        # Pre-compute deltas
        dm1 = mmax1 - mmin1
        dm2 = mmax2 - mmin2

        # Draw samples for the CDF values.
        U = random_state.uniform(size=shape)

        # Write terms for rectangular distribution.
        i_rectangular = mmax2 <= mmin1
        dm1_rectangular = dm1[i_rectangular]
        # Skip if there are no samples.
        if dm1_rectangular.size > 0:
            U_rectangular = U[i_rectangular]
            m1_samples[i_rectangular] = (
                mmin1[i_rectangular][...,None] +
                U_rectangular*dm1_rectangular[...,None]
            )


        # Write terms for triangular distribution.
        i_triangular = (mmin1 == mmin2) & (mmax1 == mmax2)
        dm2_triangular = dm2[i_triangular]
        # Skip if there are no samples.
        if dm2_triangular.size > 0:
            U_triangular = U[i_triangular]
            m1_samples[i_triangular] = (
                mmin2[i_triangular][...,None] +
                self.xpy.sqrt(U_triangular)*dm2_triangular[...,None]
            )

        return m1_samples,

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]

    def params_valid(self, parameters: Parameters) -> WhereType:
        """
        Returns a boolean array specifying where the parameters are valid.
        """
        # Unpack arguments
        mmin1 = self.xpy.asarray(parameters[self.mmin1_name])
        mmax1 = self.xpy.asarray(parameters[self.mmax1_name])
        mmin2 = self.xpy.asarray(parameters[self.mmin2_name])
        mmax2 = self.xpy.asarray(parameters[self.mmax2_name])

        params_shape = self.xpy.shape(mmin1)

        # Initialize result array and a temporary array which is reused for the
        # comparisons.
        result = self.xpy.empty(params_shape, dtype=bool)
        tmp = self.xpy.empty(params_shape, dtype=bool)

        # Write the first comparison directly to the result array.
        self.xpy.greater(mmax1, mmin1, out=result)

        # Write the remaining comparisons to `tmp`, and then update `result`
        # with the bitwise-and between it and `tmp`.
        self.xpy.greater(mmax2, mmin2, out=tmp)
        result &= tmp

        self.xpy.greater_equal(mmin1, mmin2, out=tmp)
        result &= tmp

        self.xpy.greater_equal(mmax1, mmax2, out=tmp)
        result &= tmp

        self.xpy.greater(mmin1, 0.0, out=tmp)
        result &= tmp

        self.xpy.greater(mmin2, 0.0, out=tmp)
        result &= tmp

        # TEMPORARY: Also enforce that the parameters either correspond to a
        # rectangular or triangular region of support.
        result &= (mmax2 <= mmin1) | ((mmin1 == mmin2) & (mmax1 == mmax2))

        return result
