import types
import typing

import numpy

from ...coordinate import (
    Coordinate, CoordinateSystem,
    CoordinateTransforms, transformations_nil,
)
from ...population import Population, ConditionalPopulation
from ...types import Numeric, WhereType, Observables, Parameters
from ...stats import beta_pdf_noouter

from .beta import mean_variance_to_alpha_beta

__all__ = [
    "BetaConditionalPopulation",
    "LinearTanhBetaConditionalPopulation",
    "linear_tanh_transform",
]

class BetaConditionalPopulation(ConditionalPopulation):
    r"""
    Defines a :py:class:`ConditionalPopulation`,

    .. math::
       p(x | y) = \mathrm{B}(x; \mathbb{E}[X] = f(y), \mathrm{Var}[X])

    using some parameterized function :math:`f(y)` which is implemented in
    specific sub-classes.
    """
    def __init__(
            self,
            pop_y: typing.Optional[Population],
            coord_x: Coordinate, coord_system_y: CoordinateSystem,
            loc: float=0.0,
            scale: float=1.0,
            xpy: types.ModuleType=numpy,
            rate_name: str="rate",
            variance_name: str="variance",
        ):
        coord_system_x = CoordinateSystem(coord_x)
        param_names_x = (rate_name, variance_name)

        super().__init__(
            pop_y,
            coord_system_x, coord_system_y,
            param_names_x,
        )

        self.loc = loc
        self.scale = scale
        self.xpy = xpy

        self.rate_name = rate_name
        self.variance_name = variance_name

    def y_to_mean(
            self,
            observables_y: Observables,
            parameters: Parameters,
            outer: bool,
        ) -> Numeric:
        """
        Function which converts values of :math:`y` observables into the mean of
        this beta distribution.  Must be implemented in sub-classes.
        """
        raise NotImplementedError

    def get_alpha_beta(
            self,
            observables_y: Observables,
            parameters: Parameters,
            outer: bool,
        ) -> typing.Tuple[Numeric,Numeric]:
        mean = self.y_to_mean(observables_y, parameters, outer)
        variance = self.xpy.broadcast_to(
            parameters[self.variance_name].T,
            reversed(mean.shape),
        ).T

        return mean_variance_to_alpha_beta(
            mean, variance, loc=self.loc, scale=self.scale,
        )

    def get_delta_mean(
            self,
            observables_y_lo: Observables, observables_y_hi: Observables,
            parameters: Parameters,
        ) -> typing.Tuple[Numeric,Numeric]:
        mean_lo = self.y_to_mean(observables_y_lo, parameters, False)
        mean_hi = self.y_to_mean(observables_y_hi, parameters, False)

        return mean_hi - mean_lo

    def cond_pdf(
            self,
            observables_x: Observables, observables_y: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:
        # Extract the one observable, and (alpha, beta)
        x, = observables_x
        alpha, beta = self.get_alpha_beta(observables_y, parameters, True)

        return beta_pdf_noouter(x, alpha, beta, where=where)

    def cond_rvs(
            self,
            observables_y: Observables,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
        ) -> Observables:
        if random_state is None:
            random_state = self.xpy.random.RandomState()

        # Determine alpha, beta from the parameters, and then filter out values
        # we don't want to evaluate.
        alpha, beta = self.get_alpha_beta(observables_y, parameters, False)
        params_shape = alpha.shape
        alpha, beta = alpha[where], beta[where]

        out_shape = params_shape

        # Initialize array to output samples in.
        samples = numpy.full(out_shape, self.loc, dtype=numpy.float64)

        # The initial shape of the samples we'll draw.  Note that the last axis
        # has already been possibly shrunken due to down-selecting from `where`.
        sample_shape = (alpha.size,)

        # Draw samples from a beta distribution with support on [0, 1],
        # but only where the `where` array is `True`.  Due to the way numpy
        # random samples work, we do this in the transpose-space, and then
        # transpose the result afterwards.
        beta_samples_where = random_state.beta(alpha, beta)
        samples[where] = beta_samples_where*self.scale + self.loc

        return samples,

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]


def linear_tanh_transform(y, a, b, c, y_prime, outer, xpy=numpy):
    if outer:
        delta = xpy.subtract.outer(y_prime, y).T
    else:
        delta = y_prime.T - y.T
    return (
        a.T + b.T * xpy.tanh(c.T * -delta)
    ).T


class LinearTanhBetaConditionalPopulation(BetaConditionalPopulation):
    r"""
    Defines a :py:class:`ConditionalPopulation`,

    .. math::
       p(x | y) = \mathrm{B}(x; \mathbb{E}[X] = f(y), \mathrm{Var}[X]),

    where

    .. math::
       f(y) = a + b \cdot \tanh(c \cdot (y - y')),

    which has free parameters :math:`a`, :math:`b`, :math:`c`, and :math:`y'`.
    """
    def __init__(
            self,
            pop_y: typing.Optional[Population],
            coord_x: Coordinate, coord_y: Coordinate,
            loc: float=0.0,
            scale: float=1.0,
            xpy: types.ModuleType=numpy,
            rate_name: str="rate",
            a_name: str="a",
            b_name: str="b",
            c_name: str="c",
            y_prime_name: str="y_prime",
            variance_name: str="variance",
        ):
        coord_system_y = CoordinateSystem(coord_y)

        super().__init__(
            pop_y,
            coord_x, coord_system_y,
            loc=loc, scale=scale,
            xpy=xpy,
            rate_name=rate_name,
            variance_name=variance_name,
        )

        self.a_name = a_name
        self.b_name = b_name
        self.c_name = c_name
        self.y_prime_name = y_prime_name


    def y_to_mean(
            self,
            observables_y: Observables,
            parameters: Parameters,
            outer: bool,
        ) -> Numeric:
        """
        Function which converts values of :math:`y` observables into the mean of
        this beta distribution.  Must be implemented in sub-classes.
        """
        import numpy

        y, = observables_y

        a = parameters[self.a_name]
        b = parameters[self.b_name]
        c = parameters[self.c_name]
        y_prime = parameters[self.y_prime_name]

        return linear_tanh_transform(
            y, a, b, c, y_prime,
            outer, xpy=self.xpy,
        )
