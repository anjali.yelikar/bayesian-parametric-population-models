r"""
Broken powerlaw population, with arbitrary number of components.

Formulae are derived in https://dcc.ligo.org/LIGO-T1900313
"""
import types

import numpy

from ...coordinate import (
    Coordinate, CoordinateSystem,
    CoordinateTransforms, transformations_nil,
)
from ...population import Population
from ...types import Numeric, WhereType, Observables, Parameters

def powerlaw_norm(power, lower, upper, where=True, xpy=numpy):
    """
    Computes the :math:`\phi` terms in the derivation.
    """
    ## TODO: make more efficient by doing more things in-place.

    # Convert to arrays
    power = xpy.asarray(power)
    lower = xpy.asarray(lower)
    upper = xpy.asarray(upper)
    where = xpy.asarray(where)

    # Get pre-broadcasted views of arrays, and determine final shape.
    power, lower, upper, where = xpy.broadcast_arrays(
        power, lower, upper, where,
    )
    shape = power.shape

    norm = xpy.zeros_like(power)

    # Compute on indices where ``power == -1``
    idx = xpy.asarray(xpy.equal(power, -1.0, where=where))
    # Store `log(upper) - log(lower)` in output array `norm`
    xpy.log(upper, where=idx, out=norm)
    xpy.subtract(norm, xpy.log(lower, where=idx), where=idx, out=norm)

    # Compute on indices where ``power != -1``
    xpy.invert(idx, where=where, out=idx)
    # Store `(upper**(1+power) - lower**(1+power)) / (1+power)` in output array
    # `norm`
    one_plus_power = xpy.add(1.0, power, where=idx)
    xpy.power(upper, one_plus_power, where=idx, out=norm)
    xpy.subtract(
        norm, xpy.power(lower, one_plus_power, where=idx),
        where=idx, out=norm,
    )
    xpy.divide(norm, one_plus_power, where=idx, out=norm)

    # Return final result.
    return norm


def broken_powerlaw_coeffs(
        powers, break_points,
        where=True,
        copy=True,
        xpy=numpy,
    ):
    """
    Yields the coefficients for a properly normalized and continuous broken
    powerlaw, using the efficient scheme described in
    https://dcc.ligo.org/LIGO-T1900313

    Coefficients are yielded in reversed-order.

    For better memory efficiency, set `copy=False`, and the array storing each
    yielded coefficient will be overwritten in-place when the next one is
    yielded.
    """
    # Determine number of parts in the piecewise function.
    n_parts = len(powers)

    # Broadcast arrays.
    powers_and_break_points = xpy.broadcast_arrays(
        where, *powers, *break_points,
    )
    where = powers_and_break_points[0]
    powers = powers_and_break_points[1:n_parts+1]
    break_points = powers_and_break_points[n_parts+1:]

    # Pre-compute the Delta's, by first computing `powers[i+1] - powers[i]`, and
    # then taking `break_points[i+1]` exponentiated to that value, in-place.
    Deltas = xpy.subtract(powers[1:], powers[:-1], where=where)
    xpy.power(break_points[1:-1], Deltas, where=where, out=Deltas)

    # Compute the last coefficient.

    # Initialize the sum term with the `i=n-1` contribution, as it requires no
    # product term, and initialize the product term to unity.
    sum_term = powerlaw_norm(
        powers[-1],
        break_points[-2], break_points[-1],
        where=where,
        xpy=xpy,
    )
    prod_term = xpy.ones_like(sum_term)

    # Now iterate over the remaining terms, accumulating one extra part to the
    # product term each time, and then multiplying that by `phi_i` before adding
    # it onto `sum_term`.
    for i in reversed(range(n_parts-1)):
        # Multiply on the product term.
        xpy.multiply(Deltas[i], prod_term, where=where, out=prod_term)
        # Compute `phi_i`
        phi_i = powerlaw_norm(
            powers[i],
            break_points[i], break_points[i+1],
            where=where,
            xpy=xpy,
        )
        # Compute `phi_i * prod_term` and overwrite result on `phi_i`
        xpy.multiply(phi_i, prod_term, where=where, out=phi_i)
        # Add contribution to `sum_term`
        xpy.add(sum_term, phi_i, out=sum_term)

    # C_{n-1} is now just the reciprocal of `sum_term`
    C_prev = xpy.asarray(xpy.reciprocal(sum_term, where=where))

    # Yield our last coefficent.
    yield C_prev

    # Yield all subsequent coefficients using the recurrence relation.
    for i in reversed(range(n_parts-1)):
        # Initialize array to store next result.
        # If `copy=False`, we just write to `C_prev`.
        C_next = xpy.empty_like(C_prev) if copy else C_prev

        # Compute C[i] = C[i+1] * Delta[i], and yield result.
        xpy.multiply(C_prev, Deltas[i], where=where, out=C_next)
        yield C_next

    # Done!


def broken_powerlaw_pdf(
        x,
        powers, break_points,
        where=True,
        xpy=numpy,
    ):
    obs_shape = x.shape
    param_shape = powers[0].shape
    out_shape = param_shape + obs_shape

    where = xpy.broadcast_to(where, param_shape)

    n_parts = len(powers)

    # Initialize output array
    pdf = xpy.zeros(out_shape, dtype=x.dtype)
    # Initialize index array used to identify each region:
    # `break_points[i] <= x <= break_points[i+1]`
    # as well as an extra array for intermediate calculations
    idx = xpy.zeros(out_shape, dtype=bool)
    idx_intermediate = xpy.zeros_like(idx)

    # Iterate over coefficients C[-1], ..., C[0], and compute contribution to
    # the pdf at each iteration.  Allow coeffs to be overwritten once used.
    # Note that j = n_parts - i
    coeffs = broken_powerlaw_coeffs(
        powers, break_points,
        where=where,
        copy=False,
        xpy=xpy,
    )
    for j, C_j in enumerate(coeffs):
        i = n_parts - j - 1
        # Compute indices where we want to compute the pdf contribution,
        # `break_points[i] <= x <= break_points[i+1]`
        xpy.less_equal.outer(
            break_points[i], x,
            where=where[...,None], out=idx,
        )
        xpy.greater_equal.outer(
            break_points[i+1], x,
            where=where[...,None], out=idx_intermediate,
        )
        xpy.logical_and(idx, idx_intermediate, where=where[...,None], out=idx)
        # Compute `x**powers[i]`, and store result in `pdf`, working in the
        # transpose-space so broadcasting rules work out.
        xpy.power.outer(x.T, powers[i].T, where=idx.T, out=pdf.T)
        # Multiply on the coefficient to the pdf in-place, again in the
        # transpose-space.
        xpy.multiply(pdf.T, C_j.T, where=idx.T, out=pdf.T)

    # Return the final pdf
    return pdf


def broken_powerlaw_rvs(
        n_samples,
        powers, break_points,
        where=True,
        random_state=None,
        shuffle=False,
        xpy=numpy,
    ):
    from .generic import powerlaw_rvs

    if random_state is None:
        random_state = numpy.random.RandomState()

    param_shape = powers[0].shape
    out_shape = param_shape + (n_samples,)

    where = xpy.broadcast_to(where, param_shape)

    n_parts = len(powers)

    samples = xpy.ones(out_shape)

    # Obtain fraction of samples (on average) that should come from each piece
    # of the broken powerlaw.
    coeffs = broken_powerlaw_coeffs(
        powers, break_points,
        where=where,
        copy=False,
        xpy=xpy,
    )
    phis = (
        powerlaw_norm(
            powers[i], break_points[i], break_points[i+1],
            where=where, xpy=xpy,
        )
        for i in reversed(range(n_parts))
    )

    weights = list(reversed([
        xpy.asarray(coeff * phi)
        for coeff, phi in zip(coeffs, phis)
    ]))
    total_weight = xpy.sum(weights, axis=0)
    weights = [xpy.divide(w, total_weight, out=w) for w in weights]

    # Draw samples for each realization of the parameters.
    # Would need to implement inverse CDF sampling for the full distribution to
    # vectorize this (doable, just not done yet).
    for idx in numpy.ndindex(*param_shape):
        # Skip indices where `where` is `False`
        if not where[idx]:
            continue
        # Extract weights for this one set of parameters.
        weights_idx = [w[idx] for w in weights]
        # Determine number of samples to draw from each part, by a random draw
        # from a multinomial distribution.
        n_samples_per_part = random_state.multinomial(
            n_samples, weights_idx,
        )

        # Index to keep track of where we are in samples output array.
        n_drawn = 0

        for i in range(n_parts):
            n = n_samples_per_part[i]
            n_after_draw = n_drawn + n

            power_i = powers[i][idx]
            lower_i = break_points[i][idx]
            upper_i = break_points[i+1][idx]

            samples[idx][n_drawn:n_after_draw] = powerlaw_rvs(
                n, power_i, lower_i, upper_i,
                random_state=random_state,
                xpy=xpy,
            )

            n_drawn = n_after_draw

        # Shuffle the samples if requested, so samples aren't artificially
        # grouped with the part they came from.
        if shuffle:
            i_sort = random_state.choice(n_samples, n_samples, replace=False)
            samples[idx] = samples[idx][i_sort]

    return samples


class BrokenPowerlawPopulation(Population):
    """
    A population described by a broken powerlaw with :math:`n` parts.
    """
    def __init__(
            self,
            coordinate: Coordinate, n_parts: int,
            norm_name: str="norm",
            index_name: str="alpha",
            break_point_name: str="break",
            negative_index: bool=False,
            transformations: CoordinateTransforms=transformations_nil,
            xpy: types.ModuleType=numpy,
        ):
        param_names = (
            (norm_name,) +
            tuple(index_name+str(i) for i in range(n_parts)) +
            tuple(break_point_name+str(i) for i in range(n_parts+1))
        )

        super().__init__(
            CoordinateSystem(coordinate), param_names,
            transformations=transformations,
        )

        self.n_parts = n_parts

        self.norm_name = norm_name
        self.index_name = index_name
        self.break_point_name = break_point_name

        self.negative_index = negative_index

        self.xpy = numpy

    def get_indices(self, parameters: Parameters):
        indices = [
            parameters[self.index_name+str(i)]
            for i in range(self.n_parts)
        ]

        if self.negative_index:
            indices = [-index for index in indices]

        return indices

    def get_break_points(self, parameters: Parameters):
        return [
            parameters[self.break_point_name+str(i)]
            for i in range(self.n_parts+1)
        ]

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        x, = observables

        return broken_powerlaw_pdf(
            x,
            self.get_indices(parameters), self.get_break_points(parameters),
            where=where,
            xpy=self.xpy,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Numeric:
        x_samples = broken_powerlaw_rvs(
            n_samples,
            self.get_indices(parameters), self.get_break_points(parameters),
            where=where,
            random_state=random_state,
            shuffle=False,
            xpy=self.xpy,
        )
        return x_samples,

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.norm_name]
