import typing
import numpy

from pop_models.astro_models.coordinates import (
    coordinates, transformations, lalinf_name_convert
)
from pop_models.coordinate import CoordinateSystem, CoordinateTransforms
from pop_models.detection import PreSampledDetection
from pop_models.types import Observables
from pop_models.utils import debug_verbose

__all__ = [
    "CBCPreSampledDetection",
]

class CBCPreSampledDetection(PreSampledDetection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            post_samples: Observables,
            prior_probs: typing.Optional[numpy.ndarray]=None,
            transformations: CoordinateTransforms=transformations,
            truth: typing.Optional[numpy.ndarray]=None,
            base_system_samples_truth:
              typing.Optional[typing.Tuple[
                  CoordinateSystem, Observables, typing.Optional[Observables],
              ]]=None
        ) -> None:
        super().__init__(
            coord_system,
            post_samples, prior_probs=prior_probs,
            transformations=transformations,
            truth=truth,
        )


    @classmethod
    def load(
            cls,
            filename, dtype=numpy.float64,
            truth: typing.Optional[numpy.ndarray]=None,
            coord_system: typing.Optional[CoordinateSystem]=None,
            weights_field: typing.Optional[str]=None,
            inv_weights_field: typing.Optional[str]=None,
            backend: str="lalinference",
        ):
        """
        Load a CBCPreSampledDetection from a file.  Input expected in
        standard lalinference format.  Can also provide a 'truth' argument if
        this is synthetic data where the source parameters are known.

        One can optionally provide a desired coordinate system, and only those
        parameters will be extracted.  Otherwise, all supported parameters will
        be extracted, and the `to_coords` method can be used to transform or
        down-select to any desired coordinate system.
        """
        import numpy

        debug_verbose(
            "Loading samples from {}".format(filename),
            mode="cbc_samples", flush=True,
        )

        if (weights_field is not None) and (inv_weights_field is not None):
            raise ValueError(
                "Can only provide one of 'weights_field' and "
                "'inv_weights_field'"
            )

        full_post_samples = numpy.genfromtxt(filename, dtype=dtype, names=True)
        available_coord_names = full_post_samples.dtype.names

        # If no coordinate system provided, extract everything we can.
        if coord_system is None:
            coords = []
            observables = []

            for name, coord in coordinates.items():
                lalinf_name = lalinf_name_convert.get(name)
                if lalinf_name in available_coord_names:
                    coords.append(coord)
                    observables.append(full_post_samples[lalinf_name])

            coord_system = CoordinateSystem(*coords)
            debug_verbose(
                "{} defaulted to coordinate system {}"
                .format(filename, coord_system),
                mode="cbc_samples", flush=True,
            )
        else:
            observables = []

            for coord in coord_system:
                lalinf_name = lalinf_name_convert[coord.name]
                observables.append(full_post_samples[lalinf_name])

        # Load the reference prior if one is given.
        if weights_field is not None:
            prior_probs = numpy.reciprocal(full_post_samples[weights_field])
        elif inv_weights_field is not None:
            prior_probs = full_post_samples[inv_weights_field]
        else:
            prior_probs = None

        # Return appropriate CBCPreSampledDetection object.
        return cls(
            coord_system,
            observables,
            prior_probs=prior_probs,
            truth=truth,
        )


class CommandLineTools(object):
    @classmethod
    def make_parser(cls):
        import argparse

        parser = argparse.ArgumentParser()

        subparsers = parser.add_subparsers(dest="command")

        scatter_parser = subparsers.add_parser("scatter_plot")
        scatter_parser.add_argument(
            "events",
            nargs="+", metavar="EVENT",
            help="List of event posterior files.",
        )
        scatter_parser.add_argument(
            "output_file",
            help="File to store plot in.",
        )
        scatter_parser.add_argument(
            "--coordinates",
            nargs="+", metavar="COORD", default=["m1_source", "m2_source"],
            choices=list(lalinf_name_convert.keys()),
            help="Coordinates to use in plot, default is m1_source, m2_source.",
        )
        scatter_parser.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )
        scatter_parser.set_defaults(main_func=cls.scatter_plot)

        summary_parser = subparsers.add_parser("summary")
        summary_parser.add_argument(
            "events",
            nargs="+", metavar="EVENT",
            help="List of event posterior files.",
        )
        summary_parser.add_argument(
            "--coordinates",
            nargs="+", metavar="COORD", default=["m1_source", "m2_source"],
            choices=list(lalinf_name_convert.keys()),
            help="Coordinates to summarize, default is m1_source, m2_source.",
        )
        summary_parser.set_defaults(main_func=cls.summary)

        return parser


    @classmethod
    def main(cls, raw_args=None):
        if raw_args is None:
            import sys
            raw_args = sys.argv[1:]

        cli_parser = cls.make_parser()
        cli_args = cli_parser.parse_args(raw_args)

        if cli_args.command is None:
            cli_parser.error("No command provided")

        return cli_args.main_func(cli_args)


    @classmethod
    def scatter_plot(cls, cli_args):
        import itertools
        import matplotlib as mpl
        mpl.use(cli_args.mpl_backend)
        import matplotlib.pyplot as plt

        coord_system = CoordinateSystem(*(
            coordinates[coord_name] for coord_name in cli_args.coordinates
        ))
        n_params = len(cli_args.coordinates)

        fig, axes = plt.subplots(
            n_params-1, n_params-1,
            sharex="col", sharey="row",
            figsize=(3*(n_params-1),3*(n_params-1)),
            squeeze=False,
            constrained_layout=True,
        )
        # Iterate over each event
        for event_fname in cli_args.events:
            # Open tabular file for this event.
            det = (
                CBCPreSampledDetection.load(event_fname).to_coords(coord_system)
            )
            samples, priors = det.posterior_samples(det.max_samples, 0)

            indices = range(n_params-1)
            for row, col in itertools.product(indices, repeat=2):
                ax = axes[row,col]

                # Hide extra axes
                if col > row:
                    ax.axis("off")
                    continue

                ax.scatter(
                    samples[col], samples[row+1],
                    marker=".", s=1,
                )

            for row in indices:
                axes[row,0].set_ylabel(coord_system[row+1].name)
            for col in indices:
                axes[n_params-2,col].set_xlabel(coord_system[col].name)

        fig.savefig(cli_args.output_file)

    @classmethod
    def summary(cls, cli_args):
        coord_system = CoordinateSystem(*(
            coordinates[coord_name] for coord_name in cli_args.coordinates
        ))
        n_params = len(cli_args.coordinates)

        # Print header
        print(
            "Filename",
            "Stat",
            *cli_args.coordinates,
            sep="\t",
        )

        # Iterate over each event
        for event_fname in cli_args.events:
            # Open tabular file for this event.
            det = (
                CBCPreSampledDetection.load(event_fname).to_coords(coord_system)
            )
            samples, priors = det.posterior_samples(det.max_samples, 0)

            print(
                event_fname,
                "min",
                *[numpy.min(s) for s in samples],
                sep="\t",
            )
            print(
                event_fname,
                "max",
                *[numpy.max(s) for s in samples],
                sep="\t",
            )
            print(
                event_fname,
                "median",
                *[numpy.median(s) for s in samples],
                sep="\t",
            )
            print(
                event_fname,
                "mean",
                *[numpy.mean(s) for s in samples],
                sep="\t",
            )
            print(
                event_fname,
                "stdev",
                *[numpy.std(s) for s in samples],
                sep="\t",
            )
