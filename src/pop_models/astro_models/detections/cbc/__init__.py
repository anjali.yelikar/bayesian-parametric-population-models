import typing

from ....coordinate import CoordinateSystem
from ....detection import Detection
from .pre_sampled import CBCPreSampledDetection
from .gaussian import CBCGaussianDetection
from .gaussian_process import CBCGaussianProcessDetection
from .random_forest import CBCRandomForestDetection

__all__ = [
    "load",
    "supported_types",
    "CBCPreSampledDetection",
    "CBCGaussianDetection",
    "CBCGaussianProcessDetection",
    "CBCRandomForestDetection",
]

supported_types = [
    "li_samples",
    "GWTC1",
    "gauss",
    "gp",
    "gp_sklearn",
    "rf_sklearn",
]

def load(
        file_spec: str,
        sample_coord_system: typing.Optional[CoordinateSystem]=None,
        sample_weights_field: typing.Optional[str]=None,
        sample_inv_weights_field: typing.Optional[str]=None,
    ) -> Detection:
    r"""
    Load a CBC detection given a file specifier.  Specifier should take the form
    `[type]:[path to file]`, where `[type]` can be any one of: {}

    Optionally, the type and colon can be omitted, in which case it is
    equivalent to 'li_samples'.

    If a sample-based type is used, one can use the optional
    ``sample_coord_system`` argument to specify the subset of coordinates to
    load from the file.  If not provided, all coordinates will be loaded into
    memory, which is memory inefficient.

    Also for sample-based types, either a field for weights or inverse weights
    can be specified using *one* of the optional arguments
    ``sample_weights_field`` or ``sample_inv_weights_field``.
    """.format("\n  - ".join(supported_types))

    parts = file_spec.split(":")

    if len(parts) == 1:
        det_type = "li_samples"
        filename = parts[0]
    elif len(parts) == 2:
        det_type, filename = parts
    else:
        raise ValueError(
            "File specifier '{}' cannot be parsed.  The ':' character is "
            "reserved for separating the type from the filename, and cannot be "
            "present in the filename itself.  You may need to rename the file."
            .format(file_spec)
        )

    if det_type == "li_samples":
        return CBCPreSampledDetection.load(
            filename, sample_coord_system,
            weights_field=sample_weights_field,
            inv_weights_field=sample_inv_weights_field,
            backend="lalinference",
        )
    elif det_type == "gauss":
        return CBCGaussianDetection.load(filename)
    elif det_type == "gp":
        return CBCGaussianProcessDetection.load(
            filename, backend="gp_api",
        )
    elif det_type == "gp_sklearn":
        return CBCGaussianProcessDetection.load(
            filename, backend="sklearn",
        )
    elif det_type == "rf_sklearn":
        return CBCRandomForestDetection.load(
            filename, backend="sklearn",
        )
    else:
        raise KeyError(
            "The detection type '{}' is not supported.  Must be one of: {}"
            .format(det_type, supported_types)
        )
