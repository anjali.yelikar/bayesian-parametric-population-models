def populate_subparser(subparsers):
    subparser = subparsers.add_parser("data_convert")

    subparser.add_argument(
        "input_posterior",
        help="Cleaned posteriors file from O2RandPPaper, using old format.",
    )
    subparser.add_argument(
        "output_posterior",
        help="New cleaned posteriors file to convert results into.",
    )

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Force output if output file exists.  Will overwrite file.",
    )

    subparser.set_defaults(main_func=main)


def main(cli_args):
    import h5py
    import itertools
    import numpy
    import json

    from ...posterior import H5CleanedPosteriorSamples

    with h5py.File(cli_args.input_posterior, "r") as input_file:
        samples = input_file["pos"][()]
        posterior_log_probs = input_file["log_prob"][()]

        constants = dict(input_file["constants"].attrs)
        duplicates = dict(input_file["duplicates"].attrs)

        # This contains more than just the metadata.  Will remove elements as
        # they are put in their appropriate place.
        metadata = dict(input_file.attrs)

    # Any metadata values that are stored as bytes should be converted to utf-8
    # text.
    for k in list(metadata.keys()):
        if isinstance(metadata[k], bytes):
            metadata[k] = metadata[k].decode("utf-8")

    # Set of parameter names is not stored exactly.  Instead just the variables
    # are stored, and other parameters need to be determined from constants and
    # duplicates.  Since order doesn't matter beyond the variables, we simply
    # start with the variables, and then iterate over constants and duplicates,
    # appending anything missing to the end.
    variable_names = metadata.pop("variable_names").split(",")
    param_names = variable_names.copy()
    for param_name in itertools.chain(constants.keys(), duplicates.keys()):
        if param_name not in param_names:
            param_names.append(param_name)

    # 'priors' attribute has been renamed to 'pop_prior_settings'
    metadata["pop_prior_settings"] = metadata["priors"]
    del metadata["priors"]

    # Current implementation stores rate, rather than log10_rate.  Fix this.
    if "log10_rate" in variable_names:
        # Determine location of `log10_rate`
        idx = variable_names.index("log10_rate")
        # Convert to `rate`, and update name bookkeeping.
        samples[...,idx] = 10**samples[...,idx]
        variable_names[idx] = "rate"
        param_names[idx] = "rate"

    # Also fix log10_rate in prior settings.  Do this by decoding JSON into a
    # dict, and then encoding once again, after making the needed changes.
    pop_prior_settings = json.loads(metadata["pop_prior_settings"])
    if "log10_rate" in pop_prior_settings:
        # Extract old settings.
        rate_settings = pop_prior_settings["log10_rate"]
        # Convert bounds out of log-space if provided, and correctly specify
        # that original scaling was log-uniform.
        if rate_settings["dist"] == "uniform":
            for param in ["min", "max"]:
                rate_settings["params"][param] = (
                    10**rate_settings["params"][param]
                )
            rate_settings["dist"] = "log-uniform"
        # Store settings under new name, and delete old reference.
        pop_prior_settings["rate"] = rate_settings
        del pop_prior_settings["log10_rate"]
    # Encode and store changes back into metadata.
    metadata["pop_prior_settings"] = json.dumps(pop_prior_settings)

    # Prior log probabilities were not stored in old format, so we just store
    # them as NaN.
    prior_log_probs = numpy.full_like(posterior_log_probs, numpy.nan)

    post_output = H5CleanedPosteriorSamples.from_data(
        cli_args.output_posterior,
        param_names,
        samples,
        posterior_log_probs, prior_log_probs,
        constants=constants, duplicates=duplicates, metadata=metadata,
        force=cli_args.force,
    )
    post_output.close()
