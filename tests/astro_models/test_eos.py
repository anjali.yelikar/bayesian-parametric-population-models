from pop_models.astro_models import eos
import numpy

n_points = 20

rtol, atol = 1e-5, 1e-8

gamma1_min, gamma1_max = +0.20, +2.00
gamma2_min, gamma2_max = -1.60, +1.70
gamma3_min, gamma3_max = -0.60, +0.60
gamma4_min, gamma4_max = -0.02, +0.02


def test_eos_coordinates():
    g1_grid = numpy.linspace(gamma1_min, gamma1_max, n_points)
    g2_grid = numpy.linspace(gamma2_min, gamma2_max, n_points)
    g3_grid = numpy.linspace(gamma3_min, gamma3_max, n_points)
    g4_grid = numpy.linspace(gamma4_min, gamma4_max, n_points)

    g1_mesh, g2_mesh, g3_mesh, g4_mesh = numpy.meshgrid(
        g1_grid, g2_grid, g3_grid, g4_grid,
        indexing="ij",
    )

    l1_mesh, l2_mesh, l3_mesh, l4_mesh = eos.gamma2legendre(
        g1_mesh, g2_mesh, g3_mesh, g4_mesh,
    )

    gprime1_mesh, gprime2_mesh, gprime3_mesh, gprime4_mesh = eos.legendre2gamma(
        l1_mesh, l2_mesh, l3_mesh, l4_mesh,
    )

    test1 = numpy.allclose(g1_mesh, gprime1_mesh, rtol=rtol, atol=atol)
    test2 = numpy.allclose(g2_mesh, gprime2_mesh, rtol=rtol, atol=atol)
    test3 = numpy.allclose(g3_mesh, gprime3_mesh, rtol=rtol, atol=atol)
    test4 = numpy.allclose(g4_mesh, gprime4_mesh, rtol=rtol, atol=atol)

    assert test1 and test2 and test3 and test4
