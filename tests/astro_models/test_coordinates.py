from pop_models.astro_models.coordinates import *
from pop_models.coordinate import CoordinateSystem
import numpy

seed = 4
random_state = numpy.random.RandomState(seed)
n_samples = 1000

def test_mass_transformations():
    m1_source_samples = random_state.uniform(1.0, 50.0, n_samples)
    m1_det_samples = random_state.uniform(1.0, 50.0, n_samples)

    m2_source_samples = random_state.uniform(1.0, 50.0, n_samples)
    m2_det_samples = random_state.uniform(1.0, 50.0, n_samples)

    M_source_samples = random_state.uniform(2.0, 100.0, n_samples)
    M_det_samples = random_state.uniform(2.0, 100.0, n_samples)

    Mc_source_samples = random_state.uniform(1.0, 50.0, n_samples)
    Mc_det_samples = random_state.uniform(1.0, 50.0, n_samples)

    q_samples = random_state.uniform(0.001, 1.0, n_samples)
    eta_samples = random_state.uniform(0.001, 0.25, n_samples)

    z_samples = random_state.uniform(0.001, 2.0, n_samples)

    test_m1_det_to_source = numpy.allclose(
        m1_det_samples / (1+z_samples),
        transformations[
            CoordinateSystem(m1_det_coord, z_coord),
            CoordinateSystem(m1_source_coord),
        ]([m1_det_samples, z_samples]),
    )
    assert test_m1_det_to_source

    test_m1_source_to_det = numpy.allclose(
        m1_source_samples * (1+z_samples),
        transformations[
            CoordinateSystem(m1_source_coord, z_coord),
            CoordinateSystem(m1_det_coord),
        ]([m1_source_samples, z_samples]),
    )
    assert test_m1_source_to_det

    test_m2_det_to_source = numpy.allclose(
        m2_det_samples / (1+z_samples),
        transformations[
            CoordinateSystem(m2_det_coord, z_coord),
            CoordinateSystem(m2_source_coord),
        ]([m2_det_samples, z_samples]),
    )
    assert test_m2_det_to_source

    test_m2_source_to_det = numpy.allclose(
        m2_source_samples * (1+z_samples),
        transformations[
            CoordinateSystem(m2_source_coord, z_coord),
            CoordinateSystem(m2_det_coord),
        ]([m2_source_samples, z_samples]),
    )
    assert test_m2_source_to_det

    test_M_det_to_source = numpy.allclose(
        M_det_samples / (1+z_samples),
        transformations[
            CoordinateSystem(M_det_coord, z_coord),
            CoordinateSystem(M_source_coord),
        ]([M_det_samples, z_samples]),
    )
    assert test_M_det_to_source

    test_M_source_to_det = numpy.allclose(
        M_source_samples * (1+z_samples),
        transformations[
            CoordinateSystem(M_source_coord, z_coord),
            CoordinateSystem(M_det_coord),
        ]([M_source_samples, z_samples]),
    )
    assert test_M_source_to_det

    test_Mc_det_to_source = numpy.allclose(
        Mc_det_samples / (1+z_samples),
        transformations[
            CoordinateSystem(Mc_det_coord, z_coord),
            CoordinateSystem(Mc_source_coord),
        ]([Mc_det_samples, z_samples]),
    )
    assert test_Mc_det_to_source

    test_Mc_source_to_det = numpy.allclose(
        Mc_source_samples * (1+z_samples),
        transformations[
            CoordinateSystem(Mc_source_coord, z_coord),
            CoordinateSystem(Mc_det_coord),
        ]([Mc_source_samples, z_samples]),
    )
    assert test_Mc_source_to_det

    test_msource_to_q = numpy.allclose(
        m2_source_samples/m1_source_samples,
        transformations[
            CoordinateSystem(m1_source_coord, m2_source_coord),
            CoordinateSystem(q_coord),
        ]([m1_source_samples, m2_source_samples]),
    )
    test_mdet_to_q = numpy.allclose(
        m2_det_samples/m1_det_samples,
        transformations[
            CoordinateSystem(m1_det_coord, m2_det_coord),
            CoordinateSystem(q_coord),
        ]([m1_det_samples, m2_det_samples]),
    )
