BBH (Multiple) Broken Powerlaw Mass Model
=========================================

.. argparse::
    :module: pop_models.applications.bbh.broken_powerlaw_simple.main
    :func: make_parser
    :prog: pop_models_bbh_broken_powerlaw_simple
