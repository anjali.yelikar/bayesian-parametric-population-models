BNS EoS Modeling with Gaussian Mass Model
=========================================


.. argparse::
    :module: pop_models.applications.bns.iid_gaussian_mass_with_eos.main
    :func: make_parser
    :prog: pop_models_bns_iid_gaussian_mass_with_eos
