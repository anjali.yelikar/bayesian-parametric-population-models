Combine Tabulated GW Sensitive Volumes
======================================

.. note::

   Sorry, we're currently having a technical issue with generating
   docs for this script with sphinx-argparse.

.. comment argparse
    :module: pop_models.astro_models.gw_ifo_vt
    :func: VTCombiner.make_parser
    :prog: pop_models_gw_ifo_vt_combiner
