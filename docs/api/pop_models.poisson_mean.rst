pop_models.poisson_mean module
==============================

.. automodule:: pop_models.poisson_mean
    :members:
    :undoc-members:
    :show-inheritance:
