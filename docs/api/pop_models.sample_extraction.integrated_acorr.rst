pop_models.sample_extraction.integrated_acorr module
====================================================

.. automodule:: pop_models.sample_extraction.integrated_acorr
    :members:
    :undoc-members:
    :show-inheritance:
